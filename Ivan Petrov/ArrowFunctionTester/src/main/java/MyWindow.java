import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyWindow extends JFrame {

    private int clickCounter;
    private JButton btnCounter;

    MyWindow()
    {
        setSize(300, 75);
        this.btnCounter = new JButton("Click me");
        //
//        this.btnCounter.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                clickCounter++;
//                btnCounter.setText("click # "+clickCounter);
//            }
//        });
        //
        this.btnCounter.addActionListener( e -> {
            clickCounter++;
            btnCounter.setText("click # "+clickCounter);
        });
        this.add(this.btnCounter);
    }

}
