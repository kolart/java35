import java.util.*;
import java.util.function.Predicate;
//Swing --> ManagerLayout --> Android
//JComboBox
//JTable
public class ArrowFunctionTester {
    //Arrow function Java 1.8+
    //предикаты+, stream+, замена анонимных классов+ (в переопределении сортировки)
    // замена анонимных классов в обработчиках событий+
    //Predicate in Java: предикат любая функция, которая возвращает тип boolean
    //например, определить чет или нечет.
    public static boolean isEven(int a)
    {
        return a % 2 == 1;
    }
    public static boolean isOdd(int a)
    {
        return a % 2 == 0;
    }
    public static void printItems(List<Integer>items, Predicate<Integer> fn)
    {
        for(Integer it: items)
        {
            if (fn.test(it))
            {
                System.out.print(it+" ");
            }
        }
        System.out.println();
    }
    //Sample Syntax.
    /*
    double myFunction (int a, double b){return a + b;}
    (int a, double b)  -> {return a + b;}

    применяется к коллекциям из java.util
    * */
    public static void main(String[] args)
    {
        System.out.println("Hello from ArrowFunctionTester");
        List<Integer> numbers = Arrays.asList(1,1,2,3,5,8,13,21,-1,0,1,2,3,4);
        // numbers.forEach(  (Integer it ) -> {System.out.println(it);} );
        //stream, внутри коллекции
        numbers.forEach(   it  -> System.out.print(it+" ") );
        Predicate<Integer> myFn1 = num -> (num % 2) == 0;
        printItems(numbers,myFn1);
        printItems(numbers,num -> (num % 2) == 1);
        printItems(numbers,num -> num < 0);
        printItems(numbers,num -> true);
        List<MyUser> users = new ArrayList<>();
        users.add( new MyUser(0,"Ivanov") );
        users.add( new MyUser(-1,"R2D2") );
        users.add( new MyUser(2,"Petrov") );
        users.add( new MyUser(1,"Sidorov") );
        System.out.println("before sorting:");
        users.forEach( u -> System.out.print(u+" "));
        Collections.sort(users);
        System.out.println("after def. sorting:");
        users.forEach( u -> System.out.print(u+" "));
        Collections.sort(users, (u1, u2)-> u1.getName().compareTo(u2.getName()));
//        Collections.sort(users, new Comparator<MyUser>() {
//            @Override
//            public int compare(MyUser o1, MyUser o2) {
//                return o1.getName().compareTo(o2.getName());
//            }
//        });

        System.out.println();
        System.out.println("after getName() sorting:");
        users.forEach( u -> System.out.print(u+" "));

        MyWindow window = new MyWindow();
        window.setVisible(true);

    }
}
