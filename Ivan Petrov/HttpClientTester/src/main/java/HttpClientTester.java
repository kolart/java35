
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.HttpResponse;

import java.io.IOException;
import java.util.Scanner;

public class HttpClientTester {
    public static  void main(String[] args)
    {
        System.out.println("Hello from HttpClientTester");
        //любой get запрос к сайту, который возвращает json
        //ну или html = txt
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //запрос с сайта Open weather map
        // String httpString = "https://api.openweathermap.org/data/2.5/weather?q=London&appid=3ea4abe0212f835881380b8afaa025d4";
        // https://79f3e601-01f0-418c-a351-0696f277a073.mock.pstmn.io
        // my-london-weather-test-message
        // https://79f3e601-01f0-418c-a351-0696f277a073.mock.pstmn.io/my-london-weather
        String httpString = "https://79f3e601-01f0-418c-a351-0696f277a073.mock.pstmn.io/my-london-weather";
        HttpGet httpGet = new HttpGet(httpString);
        System.out.println("Request type: "+httpGet.getMethod());
        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            Scanner sc = new Scanner(httpResponse.getEntity().getContent());
            StringBuilder result = new StringBuilder();
            while (sc.hasNext())
            {
                String line = sc.nextLine();
                System.out.println(line);
                result.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
