import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
@XmlRootElement// обяз = означает, что этот класс будет сохранятся в XML
@XmlAccessorType(XmlAccessType.NONE)
public class Users {
    @XmlElement(name = "user")//@XmlElement(name = "user")
    // @XmlElement()
    private List<User> users = new ArrayList<>();

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
