
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;
import javax.xml.bind.Marshaller;
import  javax.xml.bind.JAXBContext;
import  javax.xml.bind.JAXBException;
import org.json.XML;

/*
использовать классы из собственной библиотеки.
Перенсти смысловой класс в библиотеку или переделать уже
существующий там класс с таким же именем.
*
* */
public class XMLCreator {

    public static void main(String[] args)
    {
        System.out.println("Hello from XMLCreator");
        User user1 = new User();
        user1.setId(100);
        user1.setName("Ivan");
        user1.setAge(29);

        User user2 = new User();
        user2.setId(200);
        user2.setName("Petrov");
        user2.setAge(19);

        Users users = new Users();
        users.getUsers().add(user1);
        users.getUsers().add(user2);

        File file = new File("c:\\MyData\\users.xml");
        try {
            JAXBContext context = JAXBContext.newInstance(Users.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(users, file);// !!!
            marshaller.marshal(users, System.out);
            //
            Path path = file.toPath();
            String tmps = Files.lines(path).collect(Collectors.joining("\n"));
            org.json.JSONObject xmlTojson = XML.toJSONObject(tmps);
            String jsonString = xmlTojson.toString();
            System.out.println(jsonString);
            //ДЗ из json создать свою коллекцию User
            //
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }


    }

}
