import javax.xml.bind.annotation.*;

@XmlRootElement// обяз = означает, что этот класс будет сохранятся в XML
@XmlAccessorType(XmlAccessType.NONE)
//@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class User {

    @XmlElement
    private String name;
    @XmlElement
    int age;// default
    // @XmlAttribute(name = "IDENTITY", required = true)
    @XmlElement
    int id;//будет внутри атрибутом. атрибут будет называться IDENTITY

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", id=" + id +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User(String name) {
        this.name = name;
    }

    public User() {
    }
}
