package myobjects;

public class BeanA {
    //имена полей = названия типов в других языках программирования
    public boolean bool;
    public int integer;// int - не класс
    public String string;

    public BeanA(boolean bool, int integer, String string) {
        this.bool = bool;
        this.integer = integer;
        this.string = string;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "bool=" + bool +
                ", integer=" + integer +
                ", string='" + string + '\'' +
                '}';
    }

    public boolean isBool() {
        return bool;
    }

    public void setBool(boolean bool) {
        this.bool = bool;
    }

    public int getInteger() {
        return integer;
    }

    public void setInteger(int integer) {
        this.integer = integer;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public BeanA() {
    }
}
