package myobjects;

import java.util.List;

//MyBean - обертка коллекции (элемент Object) - любой коллекции
public class MyBean {

    private List data;//

    @Override
    public String toString() {
        return "MyBean{" +
                "data=" + data +
                '}';
    }

    public List getData() {
        return data;
    }

    public void setData(List data) {
        this.data = data;
    }

    public MyBean() {
    }
}
