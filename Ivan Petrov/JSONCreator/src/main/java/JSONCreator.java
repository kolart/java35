import myobjects.BeanA;
import myobjects.MyBean;
import myobjects.Person;
import net.sf.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/*
перейти на использование библиотеки.
Добавить классы в пакет myobject внутри библиотеки
* */
public class JSONCreator {
    public static void main(String[] args)
    {
        System.out.println("hello from JSONCreator");
        //создание класса из строки json (= ответ сервера)
        String json = "{bool:true,integer:1,string:\"jsonSTR\"}";
        JSONObject jsonObject = JSONObject.fromObject(json);
        BeanA beanA = (BeanA) jsonObject.toBean(jsonObject, BeanA.class);
        System.out.println("Create BeanA class");
        System.out.println(beanA.toString());
        //создание коллекции элементов Person
        String jsonCollection = "{'data':[{'name':'Ivan'},{'name':'Petrov'}]}";
        //{"users":{"user":[{"name":"Ivan","id":100,"age":29},{"name":"Petrov","id":200,"age":19}]}}
        //{"users":{"data":[{"name":"Ivan","id":100,"age":29},{"name":"Petrov","id":200,"age":19}]}}
        Map classMap = new HashMap();// key, value
        classMap.put("data", Person.class);//имя массива, тип элемента
        MyBean bean = (MyBean) JSONObject.toBean(
                JSONObject.fromObject(jsonCollection),//json ы данными
                MyBean.class,//общая структура json
                classMap//класс, который описывает саму коллекцию
        );
        System.out.println("inner collection:");
        System.out.println(bean);//.toString

    }
}
