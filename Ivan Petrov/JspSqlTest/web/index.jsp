<%-- 
0. импорты taglib prefix="sql"
1. Добавляем драйвер
2. бин классами и тєгами регистриуем источник данных

3. Доступ к источнику из верстки
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<!-- имя переменной - имя источника данных var="snapshot"  -->
<sql:setDataSource var="snapshot" 
                   driver="org.sqlite.JDBC"
     url="jdbc:sqlite:D:\MyData\test.db"
     user="root"  password="pass123"/>
<!-- результат запроса - набор строк - как в JDBC, var="result" -->
<sql:query dataSource="${snapshot}" var="result">
 SELECT em.*,
       d.Name as DepName 
from 
      Employees em left join EmployeesToDepartments etod on em.EmployeesId = etod.EmployeesId left join Departments d on d.DepartmentsId = etod.DepartmentsId
</sql:query>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Employees :</h1>
        <table>
            <tr>
                <th>Emp ID</th>
                <th>Name</th>
                <th>id Code</th>
                <th>Deps Namee</th>
            </tr>
            <!-- for(Row R: result.rows) -->
            <c:forEach var="R" items="${result.rows}">
                <tr>
                    <td><c:out value="${R.EmployeesId}"/></td>
                    <td><c:out value="${R.Employees_Name}"/></td>
                    <td><c:out value="${R.IdCode}"/></td>
                    <td><c:out value="${R.DepName}"/></td>
                </tr>
            </c:forEach>
        </table>
        
    </body>
</html>
