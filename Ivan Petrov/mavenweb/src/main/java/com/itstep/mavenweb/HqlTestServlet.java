package com.itstep.mavenweb;

// для работы сервлета
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;

//hibernate
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HqlTestServlet extends HttpServlet {

    private ServiceRegistry serviceRegistry;///аналог ангуляровского подхода subscribe/unsubsribe
    //регистрируем ресурсы, которые нужно освободить по требованию
    //иными словами, - нужно осовбить в нашем приложении


    public Session openSession() {

        //проект разворачивается на локальном сервере для разработчика.
        //сам сервер не конфигурировался.
        
        //Документация:
        // файл настроек Hibernate.cfg согласно документации должен быть в директории
        // которая зарегистрирована в class path
        // Для web server это его корневая. Или WEB-INF (рекомендации для всех конфигов)
        
        //исследования:
        //Что такое корневая директория?
        
//        File file = new File("my_upload.dat");
//        try {
//            FileWriter writer = new FileWriter(file);
//            writer.write("test");
//            writer.close();
//        } catch (IOException ex) {
//            Logger.getLogger(HqlTestServlet.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        //C:\Users\T530\GlassFish_Server\glassfish\domains\domain1\config
        String hibernatePropsFilePath = "hibernate_mavenweb.cfg.xml";
        File hibernatePropsFile = new File(hibernatePropsFilePath);
        boolean result = hibernatePropsFile.exists();
        if (!result) throw new HibernateException("could not find"+hibernatePropsFilePath);
        Configuration configuration = new Configuration();
        configuration.configure(hibernatePropsFile);
        StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        serviceRegistry = serviceRegistryBuilder.build();
        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        // obtains the session
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        return session;

    }

    public void closeSession(Session session) {
        session.getTransaction().commit();
        session.close();
        StandardServiceRegistryBuilder.destroy(serviceRegistry);
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // работа с Hibernate на основании примера HibernateQueryLanguageTester
        Session hibernateSession = openSession();
        // получение списка категорий
        List<Category> categories = getCategory(hibernateSession);
       
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HqlTestServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet HqlTestServlet at " + request.getContextPath() + "</h1>");
            
            addCategories(out,categories);
            
            
            out.println("</body>");
            out.println("</html>");
        }
        closeSession(hibernateSession);
    }
    
        private List<Category> getCategory(Session hibernateSession) {
                String hqlText = "from Category";
                Query q = hibernateSession.createQuery(hqlText);
                List<Category> categories = q.list();
                return categories;
        }
        
        private void addCategories(PrintWriter out, List<Category> categories) {
       
            out.println("<h2>Categories:</h2>");
            out.println("<table>");
            for(Category c : categories)
            {
                out.println("<tr><td>"+c.getName()+" </td></tr>");
            }
            out.println("</table>");
            
        }

        

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

  


}
