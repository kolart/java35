<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>


<sql:setDataSource var="snapshot" driver="org.sqlite.JDBC"
     url="jdbc:sqlite:d:\MyData\test.db"
     user="root"  password="pass123"/>

<sql:query dataSource="${snapshot}" var="result">
SELECT * from Employees;
</sql:query>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page for Employees</title>
    </head>
    <body>
        <h1>Список сотрудников :</h1>
        <table border="1" width="100%">
<tr>
<th>Emp ID</th>
<th>Name</th>
<th>id Code</th>
</tr>
<c:forEach var="row" items="${result.rows}">
<tr>
<td><c:out value="${row.EmployeesId}"/></td>
<td><c:out value="${row.Employees_Name}"/></td>
<td><c:out value="${row.IdCode}"/></td>
</tr>
</c:forEach>
</table>
        <form action="index.jsp">
            <input type="submit" value="home" />
        </form>
    </body>
</html>
