<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>

<sql:setDataSource var="snapshot" driver="org.sqlite.JDBC"
     url="jdbc:sqlite:d:\MyData\test.db"
     user="root"  password="pass123"/>
<sql:query var="emploees" dataSource="${snapshot}">
    select Employees.EmployeesId,Employees.Employees_Name from Employees
</sql:query>
    <%--
 <sql:query var="emploees" dataSource="jdbc/myDatasource">
    select Employees.EmployeesId,Employees.Employees_Name from Employees
</sql:query>
    --%>
    <%--
<sql:query var="emploees" dataSource="jdbc/mydb">
    select Employees.EmployeesId,Employees.Employees_Name from Employees
</sql:query>
    --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employeess delete</title>
    </head>
    <body>
        <h1>Employees</h1>
        <form action="EmployeesDeleteResponse.jsp">
            <select name="EmployeesId">
                <c:forEach var="row" items="${emploees.rows}">
                    
                    <option value="${row.EmployeesId}">${row.Employees_Name}</option>
                    
                </c:forEach>
            </select>
            <input type="submit" value="delete" name="btnReaderDelete" />
        </form>
        <form action="index.jsp">
            <input type="submit" value="home" />
        </form>
    </body>
</html>

