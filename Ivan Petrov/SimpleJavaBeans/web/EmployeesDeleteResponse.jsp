<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>

   
<sql:setDataSource var="snapshot" driver="org.sqlite.JDBC"
     url="jdbc:sqlite:d:\MyData\test.db"
     user="root"  password="pass123"/>

<sql:update var="delact" dataSource="${snapshot}">
        DELETE FROM Employees
        where Employees.EmployeesId = ${param.EmployeesId} 
</sql:update>

        
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employees DELETE Action</title>
    </head>
    <body>
        <h1>Удалена запись с EmployeesId = ${param.EmployeesId}</h1>
        <form action="index.jsp">
            <input type="submit" value="home" />
        </form>
    </body>
</html>