

    
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Главная страница: Simple JavaBeans</title>
    </head>
    <body>
        <h1>Список таблиц</h1>
       <table border="0">
            <thead>
                <tr>
                    <th><strong>Имя таблицы</strong> </th>
                    <th colspan="4"><strong>Операции</strong></th>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td>Employees (Сотрудники)</td>
                    <td><form action="EmployeesSelectResponse.jsp">
                            <input type="submit" value="select" name="btnSelectAuthor" />
                        </form></td>
                    <td>
                    </td>
                    <td><form action="EmployeesDelete.jsp">
                            <input type="submit" value="delete" name="btnDeleteAuthor" />
                        </form></td>
                    <td><form action="EmployeesInsert.jsp">
                            <input type="submit" value="insert" name="btnInsertAuthor" />
                        </form></td>
                </tr>
               
            </tbody>
        </table>        
    </body>
</html>

