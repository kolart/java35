<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>

 <jsp:useBean id="beanEmployeesInsert" scope="session" class="org.itstep.test.EmployeesInsertHandler" />
 
  <jsp:setProperty name="beanEmployeesInsert" property="employeesId" />
  <jsp:setProperty name="beanEmployeesInsert" property="employees_Name" />
  <jsp:setProperty name="beanEmployeesInsert" property="idCode" />
   
<sql:setDataSource var="snapshot" driver="org.sqlite.JDBC"
     url="jdbc:sqlite:D:\MyData\test.db"
     user="root"  password="pass123"/>
  
<sql:update var="updPublisher" dataSource="${snapshot}">
    
  INSERT INTO Employees 
         (IdCode,Employees_Name)
  VALUES (<jsp:getProperty name="beanEmployeesInsert" property="idCode" />, '<jsp:getProperty name="beanEmployeesInsert" property="employees_Name" />') 
    
</sql:update>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insert Employees Action Result</title>
    </head>

    <jsp:setProperty name="beanEmployeesInsert" property="idCode" />
    <jsp:setProperty name="beanEmployeesInsert" property="employees_Name" />
    <body>
        <h1>Значения IdCode = <jsp:getProperty name="beanEmployeesInsert" property="idCode" />, Employees_Name = <jsp:getProperty name="beanEmployeesInsert" property="employees_Name" /> добавлены в базу данных.</h1>
        <form action="index.jsp">
            <input type="submit" value="home" />
        </form>
    </body>
</html>
