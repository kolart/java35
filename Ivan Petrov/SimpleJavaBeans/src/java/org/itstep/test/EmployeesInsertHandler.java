/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.itstep.test;

/**
 *
 * @author Artik
 */
public class EmployeesInsertHandler {
    private String employeesId;
    private String employees_Name;
    private String idCode;
    
    public EmployeesInsertHandler(){
        this.employeesId=null;
        this.employees_Name="NoName";
        this.idCode=null;
    }
    
    public String getEmployeesId() {
        return employeesId;
    }

    public void setEmployeesId(String employeesId) {
        this.employeesId = employeesId;
    }

    public String getEmployees_Name() {
        return employees_Name;
    }

    public void setEmployees_Name(String employees_Name) {
        this.employees_Name = employees_Name;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }
}
