
package javabeanclasslibrarytester;

import mymath.Calc;
import myobjects.Man;
import helpers.MyMath;

public class JavaBeanClassLibraryTester {

  
    public static void main(String[] args) {
        System.out.println("Hello from JavaBeanClassLibraryTester");
        double area = Calc.calcGeron(3, 4, 5);
        System.out.println("S = " + area);
        Man man = new Man();
        man.setLname("Petrov");
        man.setFname("Ivan");
        System.out.println(man);
        double s = MyMath.calcArea();
    }
    
}
