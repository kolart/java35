/*������� ��� ������ (��� ��������) � ����� �� ����� ��� ������ ����� FOR*/
SET SERVEROUTPUT ON
DECLARE
  S INTEGER := -1;-- SUM INTEGER := 0; -- ������� ��� ���������
BEGIN
  S := 0; -- % --> MOD
  FOR I IN 1..10 LOOP
    IF MOD(I,2) = 0 THEN
    -- IF MOD(I,2) <> 0 THEN -- ������� ����
      BEGIN
        DBMS_OUTPUT.PUT_LINE(i||' IS ODD');
      END;
    ELSE
      BEGIN
        DBMS_OUTPUT.PUT_LINE(i||' IS EVEN');
      END;
    END IF;
    S := S + I;
  END LOOP;
  DBMS_OUTPUT.PUT_LINE('SUMM = '||TO_CHAR(S));
END;