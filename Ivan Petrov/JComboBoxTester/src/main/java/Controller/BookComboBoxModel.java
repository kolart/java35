package Controller;

import Model.Book;

import javax.swing.*;
import java.util.Vector;

public class BookComboBoxModel extends DefaultComboBoxModel<Book> {
    public BookComboBoxModel(Vector<Book> v) {
        super(v);
    }
    @Override
    public Book getSelectedItem() {
        Book selected = (Book)super.getSelectedItem();
        return selected;
    }
}
