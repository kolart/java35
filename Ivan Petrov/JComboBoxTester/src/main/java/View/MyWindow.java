package View;

import Controller.BookComboBoxModel;
import Model.Book;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.Vector;

public class MyWindow extends JFrame implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JButton) {
            JButton btn = (JButton) e.getSource();
            if (btn == btnSelect) {
                Book selectedBook = (Book)cbox.getSelectedItem();
                JOptionPane.showMessageDialog(MyWindow.this,
                        selectedBook);
            }
            if (btn == btnRemove) {

            }
        }
    }
    // JComboBox<String> cbox;
    JComboBox<Book> cbox;
    // Vector<String> books;
    Vector<Book> books;
    JButton btnSelect;
    JButton btnRemove;
   public  MyWindow()
    {
        this.setTitle("JComboBox tester");
        this.setSize(600, 75);
        this.setLayout( new FlowLayout(FlowLayout.LEFT));
//        books = new Vector<String>(100);
//        books.add("Книга 1");
//        books.add("Введение в");
//        books.add("Книга 3");
        books = new Vector<Book>(100);
        books.add(new Book("Букварь"));
        books.add(new Book("Азбука"));
        books.add(new Book("Основы программирования"));
        Collections.sort(books);
        // cbox = new JComboBox<>(books);//для коллекции строк модель не нужна.
        //модель не нужна и для коллекции классов с переопределенным toString
        BookComboBoxModel model = new BookComboBoxModel(books);
        this.cbox = new JComboBox<>();
        this.cbox.setModel(model);


        this.add(cbox);
        //
        this.btnSelect = new JButton("Выбрать");
        this.btnSelect.addActionListener(this);
        this.btnRemove = new JButton("Удалить");
        this.btnRemove.addActionListener(this);
        this.add(btnSelect);
        this.add(btnRemove);

    }


}
