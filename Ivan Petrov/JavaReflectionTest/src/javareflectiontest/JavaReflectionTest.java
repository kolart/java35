
package javareflectiontest;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;


public class JavaReflectionTest {


    public static void main(String[] args) {
        User user = new User();
        // String className = user.getClass().getName();
        // System.out.println("class name is " + className);
        //полное имя класса ИмяПакета.ИмяКласса
        String className = "javareflectiontest.User";
        System.out.println("class name is " + className);
        Class clazz;
        try {
            clazz = Class.forName(className);//это аналог констуктора
            Class superClasses =  clazz.getSuperclass();
            System.out.println("у класса есть родитель "+superClasses);
            //import java.lang.reflect.Method;
            //аналог foreach из C#
            System.out.println("Методы класса:");
            for(Method m : clazz.getMethods())
            {
                System.out.println(m.toString());
            }
            //import java.lang.reflect.Method;
             Constructor[] contrs = clazz.getConstructors();
             System.out.println("список конструкторов");
             for(Constructor c : contrs)
             {
                 System.out.println(c.toString());
             }
            //
            Method method = clazz.getMethod("getEmail");
            method.invoke(user);//this
            //
        } catch (ClassNotFoundException ex) {
            System.out.println("Нет класса с таким именем " + className);
        } catch (NoSuchMethodException ex) {
           System.out.println("Нет в классе метода с таким именем " + className);
        } catch (SecurityException ex) {
            System.out.println("Нет прав доступа к метода (??? " + className);
        } catch (IllegalAccessException ex) {
           System.out.println("Нет прав доступа к метода (access level) " + className);
        } catch (IllegalArgumentException ex) {
           System.out.println("Неверный список аргументов у метода " + className);
        } catch (InvocationTargetException ex) {
           System.out.println("Несовместимый со структурой объекта this" + className);
        }
    }
    
}
