package org.itstep.kharkov;


import org.itstep.kharkov.Tax;

public class MyObjects {
    //java - standard
    //C# - ok
    //С++ - катастрофа
    public static Tax calcTax(double salary)
    {
        Tax tax = new Tax();
        tax.pension = salary * 0.22;
        tax.prof = salary * 0.004;
        tax.pn = salary * 0.18;
        return tax;
    }
}