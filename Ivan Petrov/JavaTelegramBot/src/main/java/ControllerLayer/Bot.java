package ControllerLayer;

import DataLayer.DataBase;
import DataLayer.DataFile;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.generics.LongPollingBot;

import java.util.Date;

public class Bot extends TelegramLongPollingBot {

    public static LongPollingBot getBot() {

        return new Bot();
    }

    DataFile dataFile;
    DataBase dataBase;
    public Bot() {
        super();
        this.dataFile = new DataFile();
        this.dataBase = new DataBase();
    }

    /**
     * Метод возвращает имя бота, указанное при регистрации.
     * @return имя бота
     */
    @Override
    public String getBotUsername() {
        return "JavaBot040600_bot";
    }

    /**
     * Метод возвращает token бота для связи с сервером Telegram
     * @return token для бота
     */
    @Override
    public String getBotToken() {
        return "5407375257:AAHQfhXYJa-Oym8sAtf6D4teWtzXeOBmXH0";
    }
//
    /**
     * Метод для приема сообщений.
     * @param update Содержит сообщение от пользователя.
     */
    @Override
    public void onUpdateReceived(Update update) {
        //
        if (update.hasMessage() && update.getMessage().hasText()) {
            //

            // Set variables
            Date currentDate = new Date();
            String message_text = update.getMessage().getText();
            long chat_id = update.getMessage().getChatId();
            if (message_text.equals("/time"))
            {
                //
                String messageSend = currentDate.toString();
                sendMsg(update.getMessage().
                        getChatId().toString(), messageSend);
                //
            }else if (message_text.equals("/help"))
            {
                //
                sendMsg(update.getMessage().
                        getChatId().toString(), "две команды: /time и /hello");
                //
            } else if (message_text.equals("/files"))
            {
                //
                String fileInfo = this.dataFile.getNameFiles().toString();
                String messageSend = fileInfo;
                sendMsg(update.getMessage().
                        getChatId().toString(), messageSend);
                //
            }
            else if (message_text.equals("/employees"))
            {
                //
                String employeesInfo = this.dataBase.getEmployeesNames().toString();
                String messageSend = employeesInfo;
                sendMsg(update.getMessage().
                        getChatId().toString(), messageSend);
                //
            }
            else
            {
                String message = update.getMessage().getText();
                sendMsg(update.getMessage().getChatId().toString(), message);
            }
            //
        }
        //
        else
        {
        }
    }

//
    /**
     * Метод для настройки сообщения и его отправки.
     * @param chatId id чата
     * @param s Строка, которую необходимот отправить в качестве сообщения.
     */
    public synchronized void sendMsg(String chatId, String s) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        sendMessage.setText(s);
        try {
            sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            //log.log(Level.SEVERE, "Exception: ", e.toString());
        }
    }
//
}
