package jdbcsqlitetest;

import java.sql.DriverManager;//управление версиями драйевров

import java.sql.Connection;//механизмы соединения БД
import java.sql.Statement;//для запросов select (update)
import java.sql.CallableStatement;//для работы с ХП
import java.sql.ResultSet;//хранилище результата выборки
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JdbcSQLiteTest {


    public static void main(String[] args) {
        try {
            //+ 0. Подсоединить файл драйвера
            //+ 1. Зрегистрировать имя драйвера в проекте
            //+ 2. настроить соединене с базой данных
            //3. настроить и выполнить команду языка SQL
            //4. Обрабатывать результат вывборки (в цикле)
            //1
            Class.forName("org.sqlite.JDBC");
            //2
            Connection conn = null;
            conn = DriverManager.getConnection("jdbc:sqlite:C:\\MyData\\database.sqlite");
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT ID, TITLE FROM tutorials");
            int num = 0;
             while (rs.next())
             {
                float id = rs.getInt("ID");
                String str = rs.getString("TITLE");
                System.out.println("название  :" + str);
                System.out.println("id = "+id);
             }
            conn.close();
        
        
        } catch (ClassNotFoundException ex) {
            System.out.println("Файл драйвера не подсоединен или не от того производителя");
            //забыл jar файл добавить
            //или добавил файл для SQLite а соединяюсь с Oracle
        } catch (SQLException ex) {
            System.out.println("Неверно указан путь к базе данных");
        }
    }
    
}
