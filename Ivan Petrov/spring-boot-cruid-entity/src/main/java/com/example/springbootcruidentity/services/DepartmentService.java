package com.example.springbootcruidentity.services;

import com.example.springbootcruidentity.models.Department;
import com.example.springbootcruidentity.repositories.DepartmentRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DepartmentService {
    private DepartmentRepository departmentRepository;
    DepartmentService(DepartmentRepository departmentRepository){
        this.departmentRepository = departmentRepository;
    }

    public List<Department> getAll() {
        return this.departmentRepository.findAll();
    }
}
