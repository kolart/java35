package com.example.springbootcruidentity.services;

import com.example.springbootcruidentity.models.Employee;
import com.example.springbootcruidentity.repositories.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    private EmployeeRepository employeeRepository;
    EmployeeService(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAll() {
        return this.employeeRepository.findAll();
    }

    public List<Employee> geEmployeesFromDepartment(Integer depId) {
          return this.employeeRepository.getDepartmentsEmployee(depId);
    }
}
