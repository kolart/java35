package com.example.springbootcruidentity.services;

import com.example.springbootcruidentity.models.Client;
import com.example.springbootcruidentity.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Service
public class ClientService {
    private ClientRepository clientRepository;
    @Autowired
    ClientService(ClientRepository clientRepository)
    {
        this.clientRepository = clientRepository;
    }
    public List<Client> getAll()
    {
        return this.clientRepository.findAll();
    }

    public Optional<Client> getById(Long id) {
        // return Optional.of(this.clientRepository.getById(new BigInteger(String.valueOf(id))));
        return Optional.ofNullable(this.clientRepository.findByIdOrderByNameAsc(BigInteger.valueOf(id)));
    }
    public List<Client> findAllByEmail(String email){return  this.clientRepository.findByEmailOrderByNameAsc(email);}

    public void update(Client client) {
        this.clientRepository.save(client);
    }
    public void add(Client client) {
        this.clientRepository.save(client);
    }

    public void deleteById(Long id) {
        clientRepository.deleteById(BigInteger.valueOf(id));
    }
}
