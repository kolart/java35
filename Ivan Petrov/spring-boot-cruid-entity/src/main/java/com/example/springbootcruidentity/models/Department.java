package com.example.springbootcruidentity.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity //обязательный атрибут - Hibernate
@Table(name = "Departments") //имя проверить в базе
@SequenceGenerator(name = "Departments_Seq",
        sequenceName = "DEPARTMENTS_SEQ", //имя проверить в базе данных
        allocationSize = 50,
        initialValue = 200)
public class Department implements Serializable {



    @Id//обязательный атрибут - Hibernate
    @Column(name ="DEPARTMENT_ID", nullable = false)//обязательно, если имена не совпадают
    private Integer departmentId;
    @Column(name = "DEPARTMENT_NAME",nullable = false, length = 30 )
    private String departmentName;
    @Column(name = "LOCATION_ID" )
    private Integer locationId;
    @Column(name = "MANAGER_ID" )
    private Integer managerId;
    //отношение One-To-Many
    @OneToMany(mappedBy = "department")//departments - имя свойства в классе
    //для описания подчиненной таблицы
    private List<Employee> employeeList;

    public Department() {
    }

    public Department(Integer departmentId, String departmentName, Integer locationId, Integer managerId) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.locationId = locationId;
        this.managerId = managerId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList1) {
        this.employeeList = employeeList1;
    }

    public Employee addEmployees(Employee employee) {
        getEmployeeList().add(employee);
        employee.setDepartments(this);
        return employee;
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentId=" + departmentId +
                ", departmentName='" + departmentName + '\'' +
                ", locationId=" + locationId +
                ", managerId=" + managerId +
                ", employeeList=" + employeeList +
                '}';
    }

    public Employee removeEmployees(Employee employee) {
        getEmployeeList().remove(employee);
        employee.setDepartments(null);
        return employee;
    }
}
