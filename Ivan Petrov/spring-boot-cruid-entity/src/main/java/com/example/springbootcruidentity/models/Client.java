package com.example.springbootcruidentity.models;

import lombok.*;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Objects;

@Entity
@EqualsAndHashCode
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
@Table(name = "CLIENTS", schema = "HR", catalog = "")
public class Client {
    @GeneratedValue(strategy=GenerationType.AUTO, generator="my_seq_gen")
    @SequenceGenerator(name="my_seq_gen", sequenceName="MY_CLIENTS_SEQUENCE", allocationSize=1)
    @Id
    private BigInteger id;
    @Basic
    @Column(name = "NAME")
    private String name;
    @Basic
    @Column(name = "PHONE")
    private String phone;
    @Basic
    @Column(name = "EMAIL")
    private String email;

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
