package com.example.springbootcruidentity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCruidEntityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootCruidEntityApplication.class, args);
    }

}
