package com.example.springbootcruidentity.models;

import com.example.springbootcruidentity.models.utils.yyyyMMddDateAdapter;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * To create ID generator sequence "EMPLOYEES_ID_SEQ_GEN":
 * CREATE SEQUENCE "EMPLOYEES_ID_SEQ_GEN" INCREMENT BY 50 START WITH 1000;
 */
@Entity
@Table(name = "Employees") //имя проверить в базе данных
@SequenceGenerator(name = "Employees_Id_Seq_Gen",
        sequenceName = "EMPLOYEES_SEQ", //имя проверить в базе данных
        allocationSize = 50,
        initialValue = 200)
public class Employee implements Serializable {

    @Override
    public String toString() {
        return "Employees{" + "employeeId=" + employeeId + ", commissionPct=" + commissionPct + ", email=" + email + ", firstName=" + firstName + ", hireDate=" + hireDate + ", jobId=" + jobId + ", lastName=" + lastName + ", managerId=" + managerId + ", phoneNumber=" + phoneNumber + ", salary=" + salary + ", departments=" + department + '}';
    }

    @Id
    @Column(name ="EMPLOYEE_ID", nullable = false)
    private Integer employeeId;
    @Column(name ="COMMISSION_PCT", precision = 2,scale = 2)
    private Double commissionPct;
    private String email;
    @Column(name ="FIRST_NAME",length = 20)
    private String firstName;
    @Temporal(TemporalType.DATE)
    @Column(name ="HIRE_DATE", nullable = false)
    private Date hireDate;
    @Column(name ="JOB_ID", nullable = false,length = 25)
    private String jobId;
    @Column(name ="LAST_NAME",length = 20)
    private String lastName;
    @Column(name ="MANAGER_ID")
    private Integer managerId;
    @Column(name ="PHONE_NUMBER",length = 20)
    private String phoneNumber;
    @Column(name ="SALARY")
    private Integer salary;
    //отношение
    @ManyToOne
    @JoinColumn(name ="DEPARTMENT_ID")//DEPARTMENT_ID - столбец в базе данных
    private Department department;

    public Employee() {
    }

    public Employee(Double commissionPct, Department department, String email, Integer employeeId, String firstName,
                    Date hireDate, String jobId, String lastName, Integer managerId, String phoneNumber,
                    Integer salary) {
        this.commissionPct = commissionPct;
        this.department = department;
        this.email = email;
        this.employeeId = employeeId;
        this.firstName = firstName;
        this.hireDate = hireDate;
        this.jobId = jobId;
        this.lastName = lastName;
        this.managerId = managerId;
        this.phoneNumber = phoneNumber;
        this.salary = salary;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Double getCommissionPct() {
        return commissionPct;
    }

    public void setCommissionPct(Double commissionPct) {
        this.commissionPct = commissionPct;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @XmlJavaTypeAdapter(yyyyMMddDateAdapter.class)
    public Date getHireDate() {
        return hireDate;
    }
    @XmlJavaTypeAdapter(yyyyMMddDateAdapter.class)
    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Department getDepartments() {
        return department;
    }

    public void setDepartments(Department department) {
        this.department = department;
    }
}




