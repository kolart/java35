package com.example.springbootcruidentity.controllers;

import com.example.springbootcruidentity.models.Department;
import com.example.springbootcruidentity.services.DepartmentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DepartmentController {
    private DepartmentService departmentService;
    DepartmentController(DepartmentService departmentService)
    {
        this.departmentService = departmentService;
    }
    @GetMapping("/departments")
    ResponseEntity<List<Department>> getAll()
    {
        List<Department> list = departmentService.getAll();
        return ResponseEntity.ok(list);
    }
}
