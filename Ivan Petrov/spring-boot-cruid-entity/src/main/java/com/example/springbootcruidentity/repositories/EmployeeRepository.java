package com.example.springbootcruidentity.repositories;

import com.example.springbootcruidentity.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    @Query("SELECT e FROM Employee e where e.department.departmentId =:department_id order by e.salary asc ")
    List<Employee> getDepartmentsEmployee(@Param("department_id") Integer department_id);
}
