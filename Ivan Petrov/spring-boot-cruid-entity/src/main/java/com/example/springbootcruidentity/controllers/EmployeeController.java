package com.example.springbootcruidentity.controllers;

import com.example.springbootcruidentity.models.Employee;
import com.example.springbootcruidentity.services.EmployeeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmployeeController {
    private EmployeeService employeeService;
    EmployeeController(EmployeeService employeeService)
    {
        this.employeeService = employeeService;
    }
    @GetMapping("departments/{id}/employees")
    public ResponseEntity<List<Employee>> getEmployeesFromDepartment(@PathVariable("id") Integer depId)
    {
        List<Employee> list = employeeService.geEmployeesFromDepartment(depId);
        return ResponseEntity.ok(list);
    }
}
