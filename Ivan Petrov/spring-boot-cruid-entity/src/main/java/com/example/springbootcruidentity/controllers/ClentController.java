package com.example.springbootcruidentity.controllers;

import com.example.springbootcruidentity.models.Client;
import com.example.springbootcruidentity.models.ClientSearchValues;
import com.example.springbootcruidentity.services.ClientService;
import jdk.nashorn.internal.runtime.options.Option;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ClentController {
    private ClientService clientService;
    @Autowired
    ClentController(ClientService clientService)
    {
        this.clientService = clientService;
    }
    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    public List<Client> getClients()
    {
        return this.clientService.getAll();
    }
    @RequestMapping(value = "/clients/{id}", method = RequestMethod.GET)
    public ResponseEntity<Client> getClient(@PathVariable("id") Long id)
    {
        Optional<Client> client = clientService.getById(id);
        if (client.isPresent())
        {
            return new ResponseEntity<>(client.get(), HttpStatus.OK);
        }else
        {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    // @RequestMapping(value = "/clients/search", method = RequestMethod.POST)
    @PostMapping("/clients/search")
    public ResponseEntity<List<Client>> search(@RequestBody ClientSearchValues clientSearchValues)
    {
        List<Client> list = clientService.findAllByEmail(clientSearchValues.getEmail());
        return ResponseEntity.ok(list);
    }
    ///
    /*
    {
    "id":	2,
    "name"	:"Ivan",
    "phone"	:"380507778899",
    "email"	:"IV.PETROV@GMAIL.COM"
     }
     */
    @PutMapping("/clients/update")
    public ResponseEntity update (@RequestBody Client client)
    {
        if (client.getId() == null)
        {
            return new ResponseEntity("missed param id", HttpStatus.NOT_ACCEPTABLE);
        }
        clientService.update(client);
        return new ResponseEntity(HttpStatus.OK);
    }
    ///
    @PostMapping("/clients/add")
    public ResponseEntity<Client> add(@RequestBody Client client)
    {
        if (client.getId() != null)
        {
            return new ResponseEntity("param id should be empty", HttpStatus.NOT_ACCEPTABLE);
        }
        clientService.add(client);
        return new ResponseEntity(HttpStatus.OK);
    }
    //
    // @PutMapping("/clients/delete")
    @DeleteMapping("/clients/delete")
    public ResponseEntity delete (@RequestBody Long id)
    {
        try
        {
            clientService.deleteById(id);
        }catch (EmptyResultDataAccessException e)
        {
            e.printStackTrace();
            return new ResponseEntity("id = "+id+" not found ",HttpStatus.NOT_ACCEPTABLE);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    //

}
