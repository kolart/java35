package com.example.springbootcruidentity.repositories;


import com.example.springbootcruidentity.models.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<Client, BigInteger> {
    //findByИмяПоляВклассOrderByИмяПоляAsc
    Client findByIdOrderByNameAsc(BigInteger id);
    //findBy зарезервированное слово для рефлексии
    //OrderBy зарезервированное слово для рефлексии
    //Asc зарезервированное слово для рефлексии
    //Email поле из объекта класса Client
    //Name поле из объекта класса Client

    List<Client> findByEmailOrderByNameAsc(String email);
}
