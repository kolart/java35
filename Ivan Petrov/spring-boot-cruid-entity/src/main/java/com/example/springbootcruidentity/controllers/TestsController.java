package com.example.springbootcruidentity.controllers;

import com.example.springbootcruidentity.services.DepartmentService;
import com.example.springbootcruidentity.services.EmployeeService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Service
@RestController
@RequestMapping("test")
public class TestsController {
    private EmployeeService employeeService;
    private DepartmentService departmentService;
    TestsController(EmployeeService employeeService,
                    DepartmentService departmentService)
    {
        this.employeeService = employeeService;
        this.departmentService = departmentService;
    }
    @RequestMapping("action")
    public String testAction()
    {
        System.out.println(this.departmentService.getAll());
        System.out.println(this.employeeService.getAll());
        return "Hello, World form TestsController!";
    }
}
