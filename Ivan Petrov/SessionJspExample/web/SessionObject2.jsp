<%-- 
    Document   : SessionObject2
    Created on : Jun 11, 2022, 11:36:16 AM
    Author     : T530
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Session Object 2 Page</title>
    </head>
    <body>
        <h2>Hello from SessionObject2</h2>
        <H1>Session id: <%= session.getId() %></H1>
        <H1>Session value for "My name" <%= session.getValue("My Name") %></H1>
        
        <%
            session.invalidate();
        %>
        
    </body>
</html>
