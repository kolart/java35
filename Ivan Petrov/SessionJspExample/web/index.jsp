<%-- 
    Document   : index
    Created on : Jun 11, 2022, 11:27:29 AM
    Author     : T530
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page для тестирования обращения к сессии внутри разметки страницы</title>
    </head>
    <body>
<H1>Session id: <%= session.getId() %></H1>
<H3>
    <li>This session was created at 
    <%= session.getCreationTime() %>
    </li>
</H1>

<H3>
    <li>
        Old MaxInactiveInterval = <%= session.getMaxInactiveInterval() %>
    </li>

    <% session.setMaxInactiveInterval(5); %>
    <li>
        New MaxInactiveInterval= <%= session.getMaxInactiveInterval() %>
    </li>
</H3>

<H2>Если ссесионные объект "Мое имя(My name)" все еще доступен, то его значение не будет пустым (null):<H2>
<H3>
    <li>
        Session value for "My name" = <%= session.getAttribute("My name") %>
    </li>
</H3>

<%-- Теперь добавим объект сессии "My dog" --%>

<% session.setAttribute("My name", new String("artem.konstantinovich")); %>

<H1>My name is <%= session.getAttribute("My name") %></H1>

<%-- Рассотрим "My name" в других страницах form --%>

<form type="post" action="SessionObject2.jsp">
    <input type="submit" name="submit" value="Invalidate">
</form>

<form type="post" action="SessionObject3.jsp">
    <input type="submit" name="submit" value="Keep">
</form>


</html>
