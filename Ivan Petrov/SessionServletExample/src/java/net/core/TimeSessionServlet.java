/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.core;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class TimeSessionServlet extends HttpServlet {

boolean flag = true;

protected void doGet(HttpServletRequest req,HttpServletResponse resp)throws ServletException {

performTask(req, resp);

}

private void performTask(HttpServletRequest req,HttpServletResponse resp) throws ServletException {

HttpSession session = null;

if (flag) {

//создание сессии и установка времени инвалидации

session = req.getSession();

int timeLive = 10; //десять секунд!

session.setMaxInactiveInterval(timeLive);

flag = false;

} else {

//если сессия не существует, то ссылка на нее не будет получена

session = req.getSession(false);

}

TimeSession.go(resp, req, session);

}

}
