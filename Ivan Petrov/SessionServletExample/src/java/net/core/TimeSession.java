/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.core;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class TimeSession {

    public static void go(HttpServletResponse resp, HttpServletRequest req, HttpSession session) {

        PrintWriter out = null;

        try {

            out = resp.getWriter();

            out.write("<br> Creation Time : " + new Date(session.getCreationTime()));

            out.write("<br> Session alive! ");

            out.flush();

            out.close();

        } catch (NullPointerException e) {

//если сессия не существует, то генерируется исключение
            if (out != null) {
                out.print("Session disabled!");
            }

        } catch (IOException e) {

            e.printStackTrace();

            throw new RuntimeException("i/o failed: " + e);

        }
    }
}
