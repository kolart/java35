package javaapplicationfororacleconnection;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

import java.sql.Statement;
import java.sql.ResultSet;
import java.util.Date;
import java.sql.CallableStatement;

public class JavaApplicationForOracleConnection {

    public static void main(String[] args) {

        System.out.println("-------- Oracle JDBC Connection Testing ------");

        try {

            Class.forName("oracle.jdbc.driver.OracleDriver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();
            return;

        }

        System.out.println("Oracle JDBC Driver Registered!");

        Connection connection = null;

        try {
            //ORA-12705: Cannot access NLS data files or invalid environment specified
            java.util.Locale locale = java.util.Locale.getDefault();
            java.util.Locale.setDefault(java.util.Locale.ENGLISH);
            
            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@localhost:1521:xe", "system",
                    "123");
           java.util.Locale.setDefault(locale);
        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;

        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
            try {
                //SELECT * FROM HR.employees
                Statement st = connection.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM HR.employees");
                while (rs.next()) {
                    int id = rs.getInt("EMPLOYEE_ID");
                    Date d = rs.getDate("HIRE_DATE");
                    String name = rs.getString("FIRST_NAME");
                    float sal = rs.getFloat("SALARY");
                    System.out.println("id = " + id + " hire_date = " + d + " name = " + name + " salary = " + sal);
                }
            } catch (SQLException ex) {
                System.out.println("executeQuery Failed! Check output console");
                ex.printStackTrace();
                return;
            }

        } else {
            System.out.println("Failed to make connection!");
        }
    }
}
