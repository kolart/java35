package myobjects;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.OneToMany;

@Entity
@Table(name="CATEGORY")
public class Category implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CATEGORY_ID")
    private long id = 1L;//не совпадает с именем столбца в базе данных
    private String name;//совпадает с именем столбца в базе данных
//список товаров, которые относятся к текущей (this) категории
    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    private Set<Product> products;// ключ, значение (Первичный ключ, значение)
    
    public Set<Product> getProducts() {
        return products;
    }
    public Category() {
    }

    public Category(String name, Set<Product> products) {
        this.name = name;
        this.products = products;
    }

    public Category(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" + "id=" + id + ", name=" + name + ", products=" + products + '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public void setProducts(Set<Product> products) {
        this.products = products;
    }

}
