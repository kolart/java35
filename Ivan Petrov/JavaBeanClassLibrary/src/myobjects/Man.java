package myobjects;
import java.util.Objects;

//1.все member должны быть private/proteced
//2.все member должны быть field
//3.явно созданный конструктор
//который использует поля
//каждый класс должен сохранятся в строку
//метод toString
//каждый класс должен давать механизм различения
//одинаковых объектов для методов contains коллекции
//метод equals
//желательно. чтобы объект класса мог быть ключем
//в HashTable

//для будущего: клонирование? (после строк)
//сортировка - реализация
public class Man {
    //вложенный класс
    //видимость такая же как и у родителя
    public class Sex
    {

        public Sex() {
        }
        
        public static final int M = 0;
        public static final int W = 1;
    }

    public Man(String fname, String lname, String sname, Sex sex) {
        this.setFname(fname);
        this.setLname(lname);//setИмяПоляСЗаглавной = нужно для алгоритмов рефлексии
        this.setSname(sname);
        this.setSex(sex);
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        //if sex is ... not null
        this.sex = sex;
    }
    
    //преобразователь в строку, вызывается неявно в
    // + , println, printf (в том числе в Scanner)
    @Override
    public String toString() {
        //переписывается с использованием String.format
        return "Man{" + "fname=" + fname + ", lname=" + lname + ", sname=" + sname + ", sex=" + sex + '}';
    }
    //нужен для того, чтобы работали команды вида:
    /*
    Man man = new ...
    HashTable ht = ...
    ht[man] = new Money();
    */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.fname);
        hash = 47 * hash + Objects.hashCode(this.lname);
        hash = 47 * hash + Objects.hashCode(this.sname);
        hash = 47 * hash + Objects.hashCode(this.sex);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        //обязательная часть алгоритма
        //сам с собой
        if (this == obj) {
            return true;
        }
        //защиты от пустоты
        if (obj == null) {
            return false;
        }
        //проверка совпадения классов - для буквы L в SOLID
        //реализованное на основании Object
        //можно на основании instanseof
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        //логика = правила программирования "различного"
        final Man other = (Man) obj;
        if (!Objects.equals(this.fname, other.fname)) {
            return false;
        }
        if (!Objects.equals(this.lname, other.lname)) {
            return false;
        }
        if (!Objects.equals(this.sname, other.sname)) {
            return false;
        }
        if (!Objects.equals(this.sex, other.sex)) {
            return false;
        }
        return true;
    }
    //явный идентификатор области видимости
    //идентификатор области видимости по умолчанию = default = пакет
    //конструктор всегда создает объект в безопасном состоянии
    public Man() {
        this.setFname("");
        this.setLname("");//setИмяПоляСЗаглавной = нужно для алгоритмов рефлексии
        this.setSname("");
        this.setSex(new Sex());
    }
    
    private String fname;
    private String lname;
    private String sname;
    private Sex sex;
}
/*
private = только для этого класса
public = для всех
protected = здесь и для наследников
default = нет идентифактора
ключевого слова нет
но присваивается поумолчанию.
область = пакета.

default = protected но не передается по
наследству

*/