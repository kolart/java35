package helpers;
//пакетный класс
//это что-то служебное, для работы внутри пакет
//пакет - объединяет логически связанные понятия
//class Area
//{
//    
//}

public class MyMath {
    //только внутренний
    //официальная позиция и описание 
    //https://docs.oracle.com/javase/tutorial/java/javaOO/nested.html
    //сборщик мусор "учитывает"/"видит" внутренние классы только
    //благодаря их хранителю - внешнему классу.
    //если бы статические классы разрешалосб делать НЕвнутренними
    //они ломали бы архитектуру VM и GC
    static class Area {
        //все содержимое статическое

        public static double calcGeron(double a, double b, double c) {
            return 0;
        }

        public static double calcSquareArea(double a) {
            return 0;
        }
    }

    public static final int myPi = 3;//до конструктора

    public static double calcArea() {
        return Area.calcGeron(3, 4, 5);
    }
    //это все пакетная область видимости
    double area;
    double perimetr;
    double side;
}
