<%-- 
    Document   : index
    Created on : 04.06.2022, 9:23:58
    Author     : T530
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!-- NameHandler mybean -->
        <jsp:useBean id="mybean" scope="session" class="org.itstep.test.NameHandler" />
        <jsp:setProperty name="mybean" property="name" />
        <h1>Hello, <jsp:getProperty name="mybean" property="name" />!</h1>
        <form name="Name Input Form" action="index.jsp">
            Enter your name (Как вас зовут?):
            <input type="text" name="name" />
            <input type="submit" value="OK" />
        </form>
    </body>
</body>
</html>
