package javabeanclassexample;

import java.awt.geom.Area;
import myobjects.MyMath;

/*
Man -- физилогические харакетристик 
Person -- социальные (phone, email)

MyMath

*/
public class JavaBeanClassExample {

 public static void main_previous(String[] args) {
         //если создан явный конструктор
         //атоматически не создается конструктор
         //поумолчанию, поэтому я создаю его самостоятельно
         Man man = new Man();
         System.out.println(man);
         // Man.Sex sex = new Man.Sex();
         // ДЗ загуглить и избавиться от ошибки
         
         Man superMan = new Man("Петр", "Романов", "1, Великий", 1);
         
    }
    
    
    
//    public static void previus_main(String[] args) {
//         Man man = new Man();
//         man.fname = "Ivan";
//         man.lname = "Petrov";
//         man.sname = "Ivanovich";
//         // man.sex = new Man.Sex();
//         //import myobjects.MyMath;
//         MyMath math = new MyMath();
//         System.out.println(math.myPi);
//         math.area = 6;
//         math.perimetr = 12;
//         // math.myPi = 5;
//         System.out.println(math.myPi);
//         MyMath math2 = new MyMath();
//         System.out.println(math2.myPi);
//         // math2.myPi = 3;
//         Area area = new Area();
//         
//    }
//    
}
