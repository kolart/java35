/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbcsqliteconnectiontest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;

/**
 *
 * @author T530
 */
public class JdbcSQLiteConnectionTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Hello from JdbcSQLiteConnectionTest");
        
        MyWorker w = new MyWorker();
        
        Thread t = new Thread(w);
        t.start();
        try {
            t.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(JdbcSQLiteConnectionTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<Employee> empls = w.getEmpls();
        for(Employee e :empls )
        {
            System.out.println(e);
        }
        
    }
    
}
