
package jdbcsqliteconnectiontest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class MyWorker  extends Thread {

    public List<Employee> getEmpls() {
        return empls;
    }
    private List<Employee> empls ;
    MyWorker()
    {
       empls = new ArrayList<>(); 
    }
    
   @Override
        public void run() {
        System.out.println("call MyWorker run()");
        try {
            //1 Присоединяю драйвер нужной СУБД +
            //2. регистрирую класс драйвера в проекте
            Class.forName("org.sqlite.JDBC");
            //3.создаем Connection
             Connection conn = null;
             conn = DriverManager.getConnection("jdbc:sqlite:C:\\MyData\\test.db");
            //4.из Connection получаем команду (Statement)
            //языка SQL
            //для select, updatem insert но НЕ ГОДИТЬСЯ для ХП
            //для ХП - CollableStatement
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT ID, NAME FROM EMPLOYEE");
            //5.выполняю текст запроса
            
             while (rs.next()) {
                    //логика чтения из текстового файла
                    int id = rs.getInt("ID");
                    String name = rs.getString("NAME");
                    //System.out.println("id = " + id + " name = " + name );
             Employee e1 = new Employee(id,name);
             empls.add(e1);
             }
             
             
        } catch (ClassNotFoundException ex) 
        {
            System.out.println("Подсоединен драйвер другой СУБД!");
        } catch (SQLException ex) 
        {
           System.out.print("Файл базы данных расположен в другом каталоге");
        }
    } 
}
