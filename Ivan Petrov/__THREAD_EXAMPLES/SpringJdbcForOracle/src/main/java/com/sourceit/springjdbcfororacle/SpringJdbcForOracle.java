package com.sourceit.springjdbcfororacle;

//jdbc
import DAO.DataSourceCreater;
import DAO.Location;
import DAO.LocationRowMapper;
// 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import oracle.jdbc.OracleDriver;
//spring jdbc
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
//от оракла
import oracle.jdbc.pool.OracleDataSource;//для sqlite нет, нужно использовать "стандарт"
//
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SpringJdbcForOracle {

    //+0. jdbc connection maven
    //+1. jdbc connection Spring:
    /**
     *
     * + DataSource + file Properties + jdbcTemplaet + RowMapper (результата
     * хапроса на поле класса) + Parametr mapper
     */
    //2. DAO (Data Access object) in Spring
    //3. db.properties in Spring
    //HW - SQLite
    /*
            //oracle

//            OracleDataSource dataSource;// import oracle.jdbc.pool.OracleDataSource;//для sqlite нет, нужно использовать "стандарт"
//            dataSource = new OracleDataSource();
//            dataSource.setDriverType("oracle.jdbc.driver.OracleDriver");
//            dataSource.setURL("jdbc:oracle:thin:@localhost:1521:xe");
//            dataSource.setUser("sys as sysdba");
//            dataSource.setPassword("123");
       
//        for (Location loc : locations) {
//            System.out.println(loc);
//        }
//
    */
    public static void main(String[] args) {
        System.out.println("-------- Oracle JDBC Connection Testing ------");
        java.util.Locale locale = java.util.Locale.getDefault();
        java.util.Locale.setDefault(java.util.Locale.ENGLISH);
        SimpleDriverDataSource dataSource = DataSourceCreater.create();
        // List<Location> locations = Location.getLocations(dataSource);
        long idLocation = 1000;
        //ввели конструктор для настройки источника данных
        //операцию по получению данных вынесли в run
        //ввели метод, который возращает готовую коллекцию
        Location location = new Location(dataSource);
        Thread t = new Thread(location);
        t.start();
        try {
            t.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(SpringJdbcForOracle.class.getName()).log(Level.SEVERE, null, ex);
        }
;
        List<Location> locations = location.getLocations();
        locations.forEach(it -> System.out.println(it));
        
        java.util.Locale.setDefault(locale);
    }

}
