/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package code;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author T530
 */
public class EmailAddressHelper {
    
    private static String RECIPIENT1 = "artem.konstantinovich@ukr.net";
    private static String RECIPIENT2 = "artem.chistyakov@gmail.com"; 
    
       private static boolean notEmail(String myEmail) {
       return false;
    }
    
    public static EmailData formatEmailData(HttpServletRequest request){
        String myEmail = request.getParameter("txtEmail");
        String body = request.getParameter("txtText");//"Welcome to JavaMailTest!"
         if (myEmail == null || notEmail(myEmail)  )
            {
                myEmail =  RECIPIENT1;
            }
            if (body == null)
            {
                body = "Test message";
            }
            
        EmailData data = new EmailData();
        data.email = myEmail;
        data.body = body;
                
        return data;
    }
}
