package DAO;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
//Component - Spring аттрибу, указывает, что у нас 
//компонент. который может включатся inject во внутрь другого

@Component
public class LocationRowMapper implements RowMapper<Location> {

    @Override
    public Location mapRow(ResultSet rs, int numRow) throws SQLException {
        //
        long id = rs.getInt("LOCATION_ID");
        String STREET_ADDRESS = rs.getString("STREET_ADDRESS");
        String POSTAL_CODE = rs.getString("POSTAL_CODE");
        String CITY = rs.getString("CITY");;
        String STATE_PROVINCE = rs.getString("STATE_PROVINCE");;
        String COUNTRY_ID = rs.getString("COUNTRY_ID");

        Location loc = new Location(id,
                STREET_ADDRESS,
                POSTAL_CODE,
                CITY,
                STATE_PROVINCE,
                COUNTRY_ID);
        //
        return loc;// неполная реализация
    }

}
