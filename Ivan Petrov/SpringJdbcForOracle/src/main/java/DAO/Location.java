package DAO;

import java.util.List;
import java.util.Objects;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

//comparator
public class Location {

    public Location(Long locationId, String streetAddress, String postalCode, String city, String stateProvince, String countryId) {
        this.locationId = locationId;
        this.streetAddress = streetAddress;
        this.postalCode = postalCode;
        this.city = city;
        this.stateProvince = stateProvince;
        this.countryId = countryId;
    }

    public Location() {
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.locationId);
        hash = 89 * hash + Objects.hashCode(this.streetAddress);
        hash = 89 * hash + Objects.hashCode(this.postalCode);
        hash = 89 * hash + Objects.hashCode(this.city);
        hash = 89 * hash + Objects.hashCode(this.stateProvince);
        hash = 89 * hash + Objects.hashCode(this.countryId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Location other = (Location) obj;
        if (!Objects.equals(this.streetAddress, other.streetAddress)) {
            return false;
        }
        if (!Objects.equals(this.postalCode, other.postalCode)) {
            return false;
        }
        if (!Objects.equals(this.city, other.city)) {
            return false;
        }
        if (!Objects.equals(this.stateProvince, other.stateProvince)) {
            return false;
        }
        if (!Objects.equals(this.countryId, other.countryId)) {
            return false;
        }
        if (!Objects.equals(this.locationId, other.locationId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Location");
        sb.append("{locationId=").append(locationId);
        sb.append(", streetAddress='").append(streetAddress).append('\'');
        sb.append(", postalCode='").append(postalCode).append('\'');
        sb.append(", city='").append(city).append('\'');
        sb.append(", stateProvince='").append(stateProvince).append('\'');
        sb.append(", countryId=").append(countryId);
        sb.append('}');
        return sb.toString();
    }
    private Long locationId;
    private String streetAddress;
    private String postalCode;
    private String city;
    private String stateProvince;
    private String countryId;

    //
    
    public static final String GET_ALL = "SELECT LOCATION_ID, STREET_ADDRESS, POSTAL_CODE, CITY, STATE_PROVINCE, COUNTRY_ID FROM HR.locations";
    public static final String GET_LOCATION = "SELECT LOCATION_ID, STREET_ADDRESS, POSTAL_CODE, CITY, STATE_PROVINCE, COUNTRY_ID FROM HR.locations where LOCATION_ID = :locationId";
    //праметр в запросе или обычный символ вопрос ? (jdbc) или именной при помощи двоеточия
    
    
    
    public static List<Location> getLocations(SimpleDriverDataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
//import java.util.List;
//List - это интерфейс List (коллекция - индексированный список)
//Location - это одна строка из запроса - одна строка таблицы
//класс - это обертка над результатом запроса
        List<Location> locations = jdbcTemplate.query(Location.GET_ALL,
                new LocationRowMapper());

        return locations;
//
    }
    //
    public static List<Location> getLocations(SimpleDriverDataSource dataSource, long locationId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        List<Location> locations =
        jdbcTemplate.query( Location.GET_LOCATION,//текст запроса в котором есть параметр
                            new Object[]{locationId},//список значений для параметров внутри   запроса
                            new LocationRowMapper());//механизм мэппинга результа запрос в класс коллекции
        return locations;
    }
    //
}
