package com.sourceit.onetomanyjpamapping;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.OneToMany;

@Entity
@Table(name="PRODUCT")
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRODUCT_ID")
    private long id = 1L;//не совпадает с именем столбца
    private String name;
    private String description;
    private float price;
    //описываем отношения - то, чего нет в самйо таблице
    //или это внешний ключ
    @ManyToOne
    @JoinColumn(name = "CATEGORY_ID")
    private Category category;//объект. внутри котрого 
    //должна быть коллекция товаров
    //категория, которая содержит этот (this) товар
   
    public Category getCategory() {
        return category;
    }
    //описываем поля в таблице
    public Product() {
    }

    public Product(String name, String description, float price, Category category) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.category = category;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", description=" + description + ", price=" + price + ", category=" + category + '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }



    public void setCategory(Category category) {
        this.category = category;
    }

}
