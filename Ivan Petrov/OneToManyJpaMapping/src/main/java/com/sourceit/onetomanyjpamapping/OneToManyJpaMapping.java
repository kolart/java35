package com.sourceit.onetomanyjpamapping;

import java.util.HashSet;
import java.util.Set;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


public class OneToManyJpaMapping {

  public static void main(String[] args) {
        System.out.println("Hello from OneToManyXmlMapping!");

        SessionFactory mFctory;
        try {
            mFctory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            System.err.println("Файл Hibernate.cfg.xml или не найден или некорректной структуры");
            throw new ExceptionInInitializerError(ex);
        }

        Session session = mFctory.openSession();
        Transaction tx = session.beginTransaction();
        
        Category category = new Category("Nouts 1 ");
        Product laptop = new Product("Lenovo II", "For those who do", 2100, category);
        Product mac = new Product("Mac pro", "Html coder", 3000,category);
        
        Set<Product> products = new HashSet<Product>();
        products.add(mac);
        products.add(laptop);
        
        category.setProducts(products);
        session.save(category);
        
        tx.commit();
        
        session.close();
        
        
    }
    
}
