package com.sourceit.hibernatemavenjpamappingtest;

import java.util.Objects;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column; 
import javax.persistence.GeneratedValue; 
import javax.persistence.GenerationType; 
import javax.persistence.Table; 
import javax.persistence.UniqueConstraint; 
 
@Entity
public class User implements Serializable {
 
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", firstName=" + firstName + ", lastName=" + last_Name + '}';
    }
 
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "USER_ID")
    private Integer userId;
   //если имя не совпадает с именем столбца в базе, то нужен тэг
   @Column(name = "FIRST_NAME")// 
    private String firstName;
    private String last_Name;//иля точное совпадение (без учета регистра)
 
    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.last_Name = lastName;
    }
 
    public User() {
    }
    
   
    public String getLast_Name() {
        return last_Name;
    }
 
    public void setLast_Name(String lastName) {
        this.last_Name = lastName;
    }
 
    public String getFirstName() {
        return firstName;
    }
 
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
 
    public Integer getUserId() {
        return userId;
    }
 
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}


