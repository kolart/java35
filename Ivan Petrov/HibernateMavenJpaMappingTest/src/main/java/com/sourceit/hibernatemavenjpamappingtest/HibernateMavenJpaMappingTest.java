package com.sourceit.hibernatemavenjpamappingtest;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


public class HibernateMavenJpaMappingTest {


    public static void main(String[] args) {
        System.out.println("Hello from HibernateMavenXmlMappingTest!");

        SessionFactory mFctory;
        try {
            mFctory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            System.err.println("Файл Hibernate.cfg.xml или не найден или некорректной структуры");
            throw new ExceptionInInitializerError(ex);
        }

        Session session = mFctory.openSession();
        Transaction tx = null;

        Long employeeID = null;
        String fname = "test user";
        try {
            tx = session.beginTransaction();
            Employee employee = new Employee(fname);
            employeeID = (Long) session.save(employee);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        
        //выборка данных из таблицы User при помощи механизма HQL
        //Hibernate Query Language
        System.out.println("--- all users ---");
        session = mFctory.openSession();
        //import org.hibernate.Query;
        Query query = session.createQuery("SELECT u FROM User u");
        //если используем createQuery то User здесь имя КЛАССА
        List<User> users = query.list();
        users.forEach( u -> System.out.println(u));
        
        

    }

    
}
