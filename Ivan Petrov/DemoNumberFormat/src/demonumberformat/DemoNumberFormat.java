package demonumberformat;


import java.text.*;// * включил все
import java.util.Date;// есть еще в import java.sql.Date;
// import java.text.DateFormat;// здесь стандартные конвертеры
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DemoNumberFormat {


    public static void main(String[] args) {
        
        String str = "1.234,567";//строка, которая представляет число
        //разделитель точка - отделяет дробную часть в En и Us
        //тогда как запятая - разделитель разряда
        //Европеская - Fr - реагирует иначе
        //внешний вид принятый в US
        NumberFormat nfUs = NumberFormat.getInstance(Locale.US);
        NumberFormat nfFr = NumberFormat.getInstance(Locale.FRANCE);
        NumberFormat nfEn = NumberFormat.getInstance(Locale.ENGLISH);
        double iUs = 0;
        double iFr = 0;
        double iEn = 0;
        try {
            iUs = nfUs.parse(str).doubleValue();
            iFr = nfFr.parse(str).doubleValue();
            iEn = nfEn.parse(str).doubleValue();
            
        } catch (ParseException ex) {
            ex.printStackTrace();
            System.out.println("Ошибка преобразования форматов из строки в дробное");
        }
        System.out.printf("iUs = %f\n iFr = %f\n iEn = %f\n", iUs, iFr, iEn);
        //Console.WriteLine("${}");
        Date d = null;// тип стандартной java
        //Date есть еще и в sql (для Oracle/SQLite нужен имено она)
        String strDate = "April 23, 2021";
        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM,
                                                   Locale.US);
        try {
            d = df.parse(strDate);
        } catch (ParseException ex) {
            System.out.println("Ошибка преобразования форматов из строки в дату");
        }
        System.out.println("US date is "+d);
        df = DateFormat.getDateInstance(DateFormat.FULL, new Locale("ru","RU")  );
        System.out.println("RU date is "+df.format(d));
        //
        df = DateFormat.getDateInstance(DateFormat.FULL, new Locale("uk","UA")  );
        System.out.println("UA date is "+df.format(d));
        //en_US en_UK ru_RU
        //en - язык - english, US - страна US
        //uk_UA - uk(ranian) UA
    }
    
}
