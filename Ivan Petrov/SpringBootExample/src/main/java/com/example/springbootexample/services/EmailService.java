package com.example.springbootexample.services;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.concurrent.Future;

@Component
@Log
public class EmailService {

    private JavaMailSender sender;
    @Autowired
    EmailService(JavaMailSender sender)
    {
        this.sender = sender;
    }
    @Async
    public Future<Boolean> sendEmail(String email, String username, String emailFrom)
    {

        try{
            MimeMessage mimeMessage = sender.createMimeMessage(); // создаем не обычный текстовый документ, а HTML
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "utf-8"); // контейнер для отправки письма

            // текст письма в формате HTML
            String htmlMsg = String.format(
                    "Здравствуйте, %s<br/><br/>" +
                            "Это тестовое письмо от %s <br/><br/>",  username, emailFrom); // вместо %s будет подставляться значение в порядке следования

            mimeMessage.setContent(htmlMsg, "text/html"); // тип письма - HTML

            message.setTo(email); // email получателя
            message.setFrom(emailFrom); // обратный адрес
            message.setSubject("FYI: проверка связи"); // тема
            message.setText(htmlMsg, true); // явно надо указать, что это HTML письмо
            sender.send(mimeMessage); // отправка

            return new AsyncResult<>(true); // true - успешная отправка, оборачиваем результат в спец. объект AsyncResult

        }catch (MessagingException e)
        {
            e.printStackTrace();
        }

        return new AsyncResult<>(false);
    }

}
