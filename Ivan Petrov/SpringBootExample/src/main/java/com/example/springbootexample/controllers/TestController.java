package com.example.springbootexample.controllers;

import com.example.springbootexample.models.Location;
import com.example.springbootexample.models.MyUser;
import com.example.springbootexample.services.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//http://localhost:9181/test/action1
@Service
@RestController
@RequestMapping("test")
public class TestController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private EmailService emailService;

    @Autowired
    TestController(EmailService emailService)
    {
        this.emailService = emailService;
    }

    // http://localhost:9181/test/sqlitetest
    @RequestMapping("sqlitetest")
    public String testSqlite()
    {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS MyUser(name VARCHAR (1000))");
        jdbcTemplate.execute("INSERT INTO MyUser VALUES('Max')");
        List<MyUser> users = jdbcTemplate.query("SELECT * FROM MyUser",
                (resultSet, rowNumber) -> new MyUser(resultSet.getString("name"))

                );
        StringBuilder sb = new StringBuilder();
        users.forEach( it -> sb.append(it));
        return  sb.toString();
    }
    //http://localhost:9181/test/oracletest
    @RequestMapping("oracletest")
    public String testOracle()
    {
        String sql = Location.GET_ALL;
        List<Location> locations = jdbcTemplate.query(sql,
                BeanPropertyRowMapper.newInstance(Location.class)
                );
        StringBuilder sb = new StringBuilder();
        // locations.forEach( it -> sb.append(it));
        locations.forEach(sb::append);
        return  sb.toString();
    }

    //http://localhost:9181/test/action1
    @RequestMapping("action1")
    public String action1()
    {
        return "Hello, World! From TestController";
    }
    // http://localhost:9181/test/mailtest
    @RequestMapping("mailtest")
    public String testMail()
    {
        this.emailService.sendEmail("artem.konstantinovich@ukr.net","Artem","Alise");
        return "testMail() called";
    }

}
