/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.tddexample2;

/**
 *
 * @author artemkoltunov
 */
class Employee {

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Employee(double salary) {
        this.salary = salary;
    }

    private double salary;

    Employee() {
         double minSalary = 6500.0;
         this.salary = minSalary;
         this.calcSalary();
        }
    double calcSalary() {
         return this.salary;
        }
    public double calcTax()
    {
        /*актуальные ставки налогов*/
        double pnTax = 18.0 / 100.0 ;// link on docs paragraph
        double esvTax = 22.0  / 100.0;//ссылка на документацию
        return (this.salary * (pnTax) + this.salary  * (esvTax));
    }
}
