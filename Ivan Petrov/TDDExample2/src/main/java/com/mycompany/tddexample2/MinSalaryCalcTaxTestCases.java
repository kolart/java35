/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.tddexample2;

/**
 *
 * @author artemkoltunov
 */
public class MinSalaryCalcTaxTestCases {
     public static void test_calcTax_minSalary_constructor_with_param()
{
    Employee employee = new Employee(6500.0);
    
    double tax  = employee.calcTax();
    double taxFormula = (6500.0 * (18.0 / 100.0 ) + 6500.0  * (22.0  / 100.0));
    printTestLog("test_calcTax_minSalary_constructor_with_param", taxFormula, tax);
    
}

public static void printTestLog(String functionName, double taxFormula, double tax)
{
    StringBuilder sb = new StringBuilder();
    if ( taxFormula   == tax ){
       sb.append(functionName+" - pass");
    }else
    {
       sb.append(functionName+" - faild");
    }
    String logResult = sb.toString();
    System.out.println(logResult);
   
}


public static void test_calcTax_minSalary_default_constructor()
{
    Employee employee = new Employee();
    employee.setSalary(6500.0);
    employee.calcSalary();
    
     double tax  = employee.calcTax();
    double taxFormula = (6500.0 * (18.0 / 100.0 ) + 6500.0  * (22.0  / 100.0));
    printTestLog("test_calcTax_minSalary_default_constructor", taxFormula, tax);
   
}
 
}
