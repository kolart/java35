package com.sourceit.hibernatequerylanguagetester;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateQueryLanguageTester {

    private Session session;//отвечает за саединение с базой данных
    private ServiceRegistry serviceRegistry;//аналог ангуляровского подхода subscribe/unsubsribe
    //регистрируем ресурсы, которые нужно освободить по требованию
    //иными словами, - нужно осовбить в нашем приложении

    //
    void openSession() {
        System.out.println("openSession()");
        try {
            //если "эксепшн" здесь - значит
            //внутри hibernate.cfg.xml ошибка синтаксиса
            //например, не закрыты тэги...
            Configuration configuration = new Configuration().configure();
            serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();

            //если ошибка здесь:
            //значит отсутсвуют сами классы для "мэппинга"
            //или jpa-тэги внутри классов
            SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            // obtains the session
            session = sessionFactory.openSession();
            session.beginTransaction();
        } catch (Exception ex) {
            String errmsg = ex.toString();
            System.out.println("Подробности ошибки: " + errmsg);
        }

    }

    void closeSession() {
        System.out.println("closeSession()");
        session.getTransaction().commit();
        session.close();
        StandardServiceRegistryBuilder.destroy(serviceRegistry);
    }

    //
    public void testQuery() {
        System.out.println("test select ");
        String hql = "select c from Category c";
        //String hql = "from Category";// select * from 
        //Hinernate
        //Query
        //language
        Query query = session.createQuery(hql);
        List<Category> categories = query.list();
        for (Category category : categories) {
            System.out.println(category);
        }
    }

    public void testSearch() {
        System.out.println("testSearch");
        String hql = "from Product where category.name = 'Computer'";
        Query query = session.createQuery(hql);
        List<Product> products = query.list();
        for (Product product : products) {
            System.out.println(product);
        }
    }
    
    public void testCountQuery()
    {
        System.out.println("testCountQuery");
        String hql = "select count(name) from Product";//count, sum, avg
        Query query = session.createQuery(hql);
        List listResult = query.list();
        Number number = (Number) listResult.get(0);
        System.out.println("select count(name) = " + number.byteValue());
        
    }
    
    
    public void testQueryWithParams()
    {
            try {
            System.out.println("testQueryWithParams()");
            String hql = "from Product where description like :keyword";
            String keyword = "Html";
            Query query = session.createQuery(hql);
            query.setParameter("keyword", "%" + keyword + "%");
            //from Product where description like %New%
            List<Product> listProducts = query.list();
            //всегда создается запрос и данные приходят из БД
            //данные в БД должны быть корректы:
            //все товары должны быть отнесены к существующим категориям
            //иначе не будет создан объект Category для товара
            for (Product aProduct : listProducts) {
                System.out.println(aProduct.getName());
            }
        } catch (Exception e) {
            String errmsg = e.toString();
            System.out.println(errmsg);
        }
    }

    public static void main(String[] args) {
        HibernateQueryLanguageTester tester = new HibernateQueryLanguageTester();
        try {
            tester.openSession();
            //
            tester.testQuery();
            tester.testSearch();
            tester.testQueryWithParams();
            tester.testCountQuery();
        } catch (Exception e) {
            e.toString();
        } finally {
            tester.closeSession();
        }

        //
    }

}
