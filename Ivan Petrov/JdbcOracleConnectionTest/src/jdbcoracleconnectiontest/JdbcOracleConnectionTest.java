package jdbcoracleconnectiontest;

//сотав jdbc
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

import java.sql.Statement;//для select, update, insert
import java.sql.ResultSet;//"аналог" DataTable из ADO.Net
import java.util.Date;
import java.sql.CallableStatement;//для вызова ХП

//служебные
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JdbcOracleConnectionTest {

    public static void main(String[] args) {
        //создаем объект класса Connection
        Connection connection = null;
        try {
            //подключаем драйвер jdbc ,например, ojdbc6.jar
            //регистрируем класс драйвера в приложении
            System.out.println("-------- Oracle JDBC Connection Testing ------");
            Class.forName("oracle.jdbc.driver.OracleDriver");
            System.out.println("Oracle JDBC Driver Registered!");

            //ORA-12705: Cannot access NLS data files or invalid environment specified
            java.util.Locale locale = java.util.Locale.getDefault();
            java.util.Locale.setDefault(java.util.Locale.ENGLISH);

            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@localhost:1521:xe", "system",
                    "123");

            java.util.Locale.setDefault(locale);
            System.out.println("Соединение с базой данных оракл установено!");
            //из соединения создаем Statement
            //для выполнения запросов SQL
            //
            Statement st = connection.createStatement();
            String sqlText
                    = "SELECT "
                    + "EMP.EMPLOYEE_ID "
                    + ",EMP.HIRE_DATE "
                    + ",EMP.FIRST_NAME "
                    + ",EMP.LAST_NAME "
                    + ",EMP.EMAIL "
                    + ",EMP.PHONE_NUMBER "
                    + ",EMP.SALARY "
                    + ",J.JOB_TITLE "
                    + ",DEP.DEPARTMENT_NAME "
                    + ",M.FIRST_NAME AS MANAGER_FNAME "
                    + "FROM HR.EMPLOYEES EMP "
                    + "LEFT JOIN HR.JOBS J ON J.JOB_ID = EMP.JOB_ID "
                    + "LEFT JOIN HR.DEPARTMENTS DEP ON DEP.DEPARTMENT_ID=EMP.DEPARTMENT_ID "
                    + "LEFT JOIN HR.EMPLOYEES M ON M.EMPLOYEE_ID = EMP.MANAGER_ID ";
            //
            ResultSet rs = st.executeQuery(sqlText);
            while (rs.next()) {
                int id = rs.getInt("EMPLOYEE_ID");
                Date d = rs.getDate("HIRE_DATE");
                String fname = rs.getString("FIRST_NAME");
                String lname = rs.getString("LAST_NAME");
                float sal = rs.getFloat("SALARY");
                String depName = rs.getString("DEPARTMENT_NAME");;
                String managerName = rs.getString("MANAGER_FNAME");;
                String jobName = rs.getString("JOB_TITLE");
                System.out.println("id = " + id + " hire_date = " + d + " name = " + fname + " salary = " + sal);
            }
            // test_call_sp(connection);
            System.out.println("Create statement for HR.create_client(name_client,phone_client,email_client);");
           //R := HR.function_for_test(A,B,C);// синтаксис в Oracle
          //                1                         2 3 4
           String sqlSP = "{? = call HR.create_client(?,?,?)}";//не java, это псевдокод ODBC (синтаксис Microsoft 1995)
        //нумерация всех параметров начинается от ЕДИНИЦЫ
        CallableStatement stmt;
        try
        {
            stmt = connection.prepareCall(sqlSP);//"компилируется" выражение псевдокода
            stmt.registerOutParameter(1, java.sql.Types.INTEGER);
            String name = "client 13";
            String phone = "000-000-11-11";
            String email = "cln13@gmail.com";
            stmt.setString(2, name);
            stmt.setString(3, phone);
            stmt.setString(4, email);
            stmt.execute();//обращение к серверу при помощи настроенного соединения
            int result = stmt.getInt(1);
            System.out.println("id = "+result);
        }catch(SQLException exSp)
        {
            System.out.println("Ошибка в вызове HR.create_client"+exSp);
        }
            
            //
        } catch (ClassNotFoundException ex) {
            System.out.printf("Не найден класс драйвера для Оракла. %s", ex.toString());

        } catch (SQLException ex) {
            System.out.printf("Проблемы соединения с сервером. %s", ex.toString());

        }
       
        //
    }

    private static void test_call_sp(Connection connection) {
        //
        System.out.println("Create statement for hr.funct_for_test");
        //R := HR.function_for_test(A,B,C);// синтаксис в Oracle
        //               1                             2 3 4
        String sqlSP = "{? = call HR.function_for_test(?,?,?)}";//не java, это псевдокод ODBC (синтаксис Microsoft 1995)
        //нумерация всех параметров начинается от ЕДИНИЦЫ
        CallableStatement stmt;
        try
        {
            stmt = connection.prepareCall(sqlSP);//"компилируется" выражение псевдокода
            stmt.registerOutParameter(1, java.sql.Types.VARCHAR);
            int a = 3;
            int b = 7;
            stmt.setInt(2, a);
            stmt.setInt(3, b);
            stmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            stmt.execute();//обращение к серверу при помощи настроенного соединения
            String result = stmt.getString(1);
            String outParam = stmt.getString(4);
            System.out.println("a + b = "+result+" , a | b = "+outParam);
        }catch(SQLException exSp)
        {
            System.out.println("Ошибка в вызове HR.function_for_test"+exSp);
        }
        //
        System.out.println("Create statement for HR.procedure_for_test(A,B,C)");
        //HR.procedure_for_test(A,B,C);;// синтаксис в Oracle
        //                                            1 2 3
        String sqlProc = "{call HR.procedure_for_test(?,?,?)}";//не java, это псевдокод ODBC (синтаксис Microsoft 1995)
        CallableStatement stProc;
        try
        {
            stProc = connection.prepareCall(sqlProc);
            int a = 3;
            int b = 7;
            stProc.setInt(1, a);
            stProc.setInt(2, b);
            stProc.registerOutParameter(3, java.sql.Types.VARCHAR);
            stProc.execute();//обращение к серверу при помощи настроенного соединения
            String outParam = stProc.getString(3);
            System.out.println(" a | b = "+outParam);
        }catch(SQLException exSp)
        {
            System.out.println("Ошибка в вызове HR.procedure_for_test"+exSp);
        }
        //
    }

}
