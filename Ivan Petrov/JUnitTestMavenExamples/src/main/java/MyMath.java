public class MyMath {
    private final double myPi = 3.14;

    public double getMyPi() {
        return myPi;
    }

    @Override
    public String toString() {
        return "MyMath{" +
                "myPi=" + myPi +
                '}';
    }

    public MyMath() {
        System.out.println("call constructor MyMath");
    }
}
