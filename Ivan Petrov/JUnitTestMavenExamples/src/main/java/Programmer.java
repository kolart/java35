public class Programmer extends Employee {
    double rate;
    double hours;
    public Programmer(double rate, double workedHours) {
        // super.setSalary(rate * workedHours);//для корректного конструктора родителя
        this.hours = workedHours;
        this.rate = rate;
        }

    @Override
    double calcSalary() {
        return this.rate * this.hours;
    }
}
