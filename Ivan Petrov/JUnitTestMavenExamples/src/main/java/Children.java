public class Children extends Parent {
    int age;

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Children{" +
                "age=" + age +
                '}';
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Children(String name, int age) {
        super(name);
        this.age = age;
    }
}
