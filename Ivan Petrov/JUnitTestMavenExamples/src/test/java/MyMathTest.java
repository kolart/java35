import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;

import static org.junit.jupiter.api.Assertions.*;

class MyMathTest {

    private  MyMath math;

    @BeforeAll
    public static void setUp() {
        System.out.println("call setUp()");
    }

    @AfterAll
    public static void tearDown() {
        System.out.println("call tearDown()");
    }


    @BeforeEach
    void setUpTest() {
        System.out.println("call setUpTest()");
        this.math = new MyMath();
    }

    @AfterEach
    void tearDownTest() {
        System.out.println("call tearDownTest()");
    }

    @Test
    void test_call_ToString_pass() {
        System.out.println("call testToString()");
        double myPi = math.getMyPi();
        String expected = "MyMath{" +
                "myPi=" + myPi +
                '}';
        String result = math.toString();
        assertTrue( expected.contains(result));
    }
    // {unit-of-work} _ {scenario} _ {expected-results-or-behaviour}
    @Test
    void test_call_getMyPi_expected_3_14_pass() {
        double expected = 3.14;
        double result = this.math.getMyPi();
        assertTrue( expected == result);
    }
    @Test
    void test_call_getMyPi_expected_fail_pass() {
        double expected = 3.00;
        double result = this.math.getMyPi();
        assertTrue( expected != result);
    }
}