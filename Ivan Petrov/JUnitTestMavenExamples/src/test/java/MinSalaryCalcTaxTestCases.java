import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinSalaryCalcTaxTestCases {
    // {unit-of-work} _ {scenario} _ {expected-results-or-behaviour}

    // {unit-of-work} == test_Employee_use_constructor_with_param
    // {scenario} == calcTax_from_minSalary
    // {expected-results-or-behaviour} == _expected_2600_0_pass
    @Test
    void test_Employee_use_constructor_with_param_calcTax_from_minSalary_expected_2600_0_pass()
    {
        Employee employee = new Employee(6500.0);
        double tax  = employee.calcTax();
        double taxFormula = (6500.0 * (18.0 / 100.0 ) + 6500.0  * (22.0  / 100.0));
        assertTrue(taxFormula == tax);
    }
    @Test
    void test_Employee_use_default_constructor_calcTax_minSalary_expected_2600_0_pass()
    {
        Employee employee = new Employee();
        employee.setSalary(6500.0);
        employee.calcSalary();

        double tax  = employee.calcTax();
        double taxFormula = (6500.0 * (18.0 / 100.0 ) + 6500.0  * (22.0  / 100.0));
        assertTrue(taxFormula == tax);
    }
}