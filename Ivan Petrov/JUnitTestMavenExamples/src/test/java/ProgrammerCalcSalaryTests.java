import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// TDD:
//Сначала "придумываю тест" (Тест план и тест кейс)
//Пишу код - как я хочу, чтобы выглядело тестирование в код
// создаю объект тестирования (Смысловой класс)
//созданные тест должен "валиться" - не пройти - краш (успех с точки зрения тестировщика, проавл для программиста)
// дописываю прошлый тест - создаю код, который тест проходит
//рефакторинг кода - преобразовываю в соответстии с требованиями архитектуры и код стайл гайда

//ДЗ добавить тесты: проверка родителя, проверка наличия методов
//(Children/Parent)
class ProgrammerCalcSalaryTests {
    // {unit-of-work} _ {scenario} _ {expected-results-or-behaviour}
    //Examples:
    // {unit-of-work} == test_Employee_use_constructor_with_param
    // {scenario} == calcTax_from_minSalary
    // {expected-results-or-behaviour} == _expected_2600_0_pass
    @Test
    void test_Employee_calc_salary_rate_hour_expected_1600_pass()
    {
        //$10 per h * 160 h
        double rate = 10;
        double workedHours = 160;
        Programmer developer = new Programmer(rate,workedHours);
        double salary = developer.calcSalary();
        double expected = rate * workedHours;
        assertTrue( expected == salary);
    }

    @Test
    void test_Employee_calc_tax_expected_640_pass()
    {
        //$10 per h * 160 h
        double rate = 10;
        double workedHours = 160;
        Programmer developer = new Programmer(rate,workedHours);
        developer.setSalary(developer.calcSalary());// катастофа в архитектуре
        //решение - введение интерфейса.
        //рефакторинг: класс Employee в интерфейс и от него наследуем.
        //есть сотрудник с оплатой час * количество часов (Programmer, Уборщик)
        //база + процент (Sales manager)
        //оклад за выполение обязанностей = бухгалтер
        double tax = developer.calcTax();
        double expected = 1600 * 0.40;
        assertTrue( expected == tax);
    }
}