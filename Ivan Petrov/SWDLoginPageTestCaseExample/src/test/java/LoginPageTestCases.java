import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.jupiter.api.Assertions.*;

class LoginPageTestCases {
    // {unit-of-work} _ {scenario} _ {expected-results-or-behaviour}
    //Examples:
    // {unit-of-work} == test_Employee_use_constructor_with_param
    // {scenario} == calcTax_from_minSalary
    // {expected-results-or-behaviour} == _expected_2600_0_pass
    // @Test
    void test_getAddress_expect_actual_url_expect_fail_pass()
    {
        System.setProperty("webdriver.gecko.driver","C:\\WebDrivers\\geckodriver.exe"); // Setting system properties of FirefoxDriver
        WebDriver driver = new FirefoxDriver();
        String url = "https://developers.minfin.com.ua/login/";
        LoginPage login = new LoginPage(driver,"MIN FIN", url);
        login.go();
        String result = login.getAddress();
        String expected = url;
        assertTrue( result != expected);
    }
    @Test
    void test_page_loaded_can_use_elements_expected_fail_pass()
    {
        //случай, кода была переадресация
        System.setProperty("webdriver.gecko.driver","C:\\WebDrivers\\geckodriver.exe"); // Setting system properties of FirefoxDriver
        WebDriver driver = new FirefoxDriver();
        String url = "https://developers.minfin.com.ua/login/";
        LoginPage login = new LoginPage(driver,"MIN FIN", url);
        login.go();
        boolean isLoginPage = login.canUseElements();
        assertTrue(!isLoginPage);
    }
    @Test
    void test_page_loaded_can_use_elements_expected_pass_pass()
    {
        //случай, кода перешли со страницы регистрации на логин
        System.setProperty("webdriver.gecko.driver","C:\\WebDrivers\\geckodriver.exe"); // Setting system properties of FirefoxDriver
        WebDriver driver = new FirefoxDriver();
        String url = "https://developers.minfin.com.ua/login/";
        LoginPage login = new LoginPage(driver,"MIN FIN", url);
        login.go();
        login.navigateToLogin();
        boolean isLoginPage = login.canUseElements();
        assertTrue(isLoginPage);
    }

}