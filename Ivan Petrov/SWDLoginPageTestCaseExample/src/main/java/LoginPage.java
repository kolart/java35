import org.openqa.selenium.*;

// содержит элементы станицы, которые тестируем автоматически
// ? возможно, содержит механизмы определени. что все ок
public class LoginPage extends PageObject {

    WebElement txtLogin;
    WebElement txtPassword;
    WebElement btnLogin;

    public LoginPage(WebDriver driver, String title, String address) {
        super(driver, title, address);
    }

    public void navigateToLogin()
    {
        WebElement link = driver.findElement(By.cssSelector("a[href='/login/']"));
        if (link != null)
        {
            System.out.println("Have link "+link);
            try {
                PageObject.makePrintScreen((TakesScreenshot) driver);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            link.click();
        }
    }

    @Override
    public boolean canUseElements() {
        try
        {
            txtLogin = driver.findElement(By.name("login"));
            txtPassword = driver.findElement(By.name("password"));
            btnLogin = driver.findElement(By.cssSelector("button[type='submit']"));
            return true;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }

    public boolean signIn() {
        boolean loginActionsCompleted = doLoginActions();
        if (loginActionsCompleted)
        {
            String actual = this.getAddress();
            String expected = "https://developers.minfin.com.ua/";
            return  expected.contains(actual);
        }
        return false;
    }

    private boolean doLoginActions() {
        try {
            PageObject.makePrintScreen((TakesScreenshot) driver);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (txtLogin != null)
        {
            txtLogin.sendKeys("artem.konstantinovich@ukr.net"+ Keys.ENTER);
        }
        if (txtPassword != null)
        {
            // txtPassword.sendKeys("Password123_"+ Keys.ENTER);
            txtPassword.sendKeys("Password123_");
        }
        if (btnLogin != null)
        {
            btnLogin.click();
            return true;
        }
        return false;
    }
}
