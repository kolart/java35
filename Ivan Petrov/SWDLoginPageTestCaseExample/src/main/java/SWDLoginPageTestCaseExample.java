import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

// TDD:
//Сначала "придумываю тест" (Тест план и тест кейс)
//Пишу код - как я хочу, чтобы выглядело тестирование в код
// создаю объект тестирования (Смысловой класс)
//созданные тест должен "валиться" - не пройти - краш (успех с точки зрения тестировщика, проавл для программиста)
// дописываю прошлый тест - создаю код, который тест проходит

// {unit-of-work} _ {scenario} _ {expected-results-or-behaviour}
//Examples:
// {unit-of-work} == test_Employee_use_constructor_with_param
// {scenario} == calcTax_from_minSalary
// {expected-results-or-behaviour} == _expected_2600_0_pass

public class SWDLoginPageTestCaseExample {

    public static void main(String[]args) throws InterruptedException {
        System.out.println("Hello from SWDLoginPageTestCaseExample");
        System.setProperty("webdriver.gecko.driver","C:\\WebDrivers\\geckodriver.exe"); // Setting system properties of FirefoxDriver
        WebDriver driver = new FirefoxDriver();
        String url = "https://developers.minfin.com.ua/login/";
        LoginPage login = new LoginPage(driver,"MIN FIN", url);
        login.go();
        login.navigateToLogin();
        login.PrintScreenToFile();
        String act =  driver.getCurrentUrl();
        boolean isLoginPage = login.canUseElements();
        // if (!isLoginPage)
        {
           // login.navigateToLogin();
            System.out.println("Заголовок страницы "+ login.getTitle());
            System.out.println("загруженный адрес (url) сайта "+ login.getAddress());
            boolean isLogin = login.signIn();
            if (isLogin)
            {
                System.out.println("загруженный адрес (url) сайта "+ login.getAddress());
            }
            System.out.println("login state is "+ isLogin);
        }

        //https://developers.minfin.com.ua/
        // testLoginFlowForBrowserstack(driver);
        // testLoginFlowForMinFin(driver);

        Thread.sleep(5000);// 5 sec pause
        System.out.println("Finished.");

    }

    private static void testLoginFlowForBrowserstack(WebDriver driver) {
        //особенность окна предупреждения - используем куки
        String url = "https://www.browserstack.com/users/sign_in";
        driver.get(url);
        System.out.println("Заголовок страницы "+ driver.getTitle());
        System.out.println("загруженный адрес (url) сайта "+ driver.getCurrentUrl());

        driver.findElement(By.id("accept-cookie-notification")).click();

        WebElement txtUserName = driver.findElement(By.id("user_email_login"));
        WebElement txtPassword = driver.findElement(By.id("user_password"));
        WebElement btnLogin = driver.findElement(By.name("commit"));

        if (txtUserName != null)
        {
            txtUserName.sendKeys("artem.konstantinovich@ukr.net"+ Keys.ENTER);
        }
        if (txtPassword != null)
        {
            txtPassword.sendKeys("Password123_");
        }
        if (btnLogin != null)
        {
            btnLogin.click();
        }
    }

    private static void testLoginFlowForMinFin(WebDriver driver) throws InterruptedException {
        // особенность переадресация страницы (начинаем с регистрации)
        //artem.konstantinovich@ukr.net
        //Password123_
        //
        String url = "https://developers.minfin.com.ua/login/";
        driver.get(url);
        System.out.println("Заголовок страницы "+ driver.getTitle());
        System.out.println("загруженный адрес (url) сайта "+ driver.getCurrentUrl());

        WebElement link = driver.findElement(By.cssSelector("a[href='/login/']"));
        if (link != null)
        {
            System.out.println("Have link "+link);
            PageObject.makePrintScreen((TakesScreenshot) driver);
            link.click();
        }

        WebElement txtLogin = driver.findElement(By.name("login"));
        WebElement txtPassword = driver.findElement(By.name("password"));
        WebElement btnLogin = driver.findElement(By.cssSelector("button[type='submit']"));

        PageObject.makePrintScreen((TakesScreenshot) driver);

        if (txtLogin != null)
        {
            txtLogin.sendKeys("artem.konstantinovich@ukr.net"+ Keys.ENTER);
        }
        if (txtPassword != null)
        {
            txtPassword.sendKeys("Password123_"+ Keys.ENTER);
        }
        if (btnLogin != null)
        {
            btnLogin.click();
        }
    }


}
