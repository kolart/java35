import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

//Представляет общие. что связано с SWD
public class PageObject {

    protected WebDriver driver;

    public String getTitle() {
        return title;
    }

    public PageObject(WebDriver driver, String title, String address) {
        this.driver = driver;
        setTitle(title);
        setAddress(address);
        this.initPage();

    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    protected String title;
    protected String address;

    public static void makePrintScreen(TakesScreenshot driver) throws InterruptedException {
        //
        Thread.sleep(2000);// 2 sec pause
        //
        Date dateNow = new Date();
        SimpleDateFormat format = new SimpleDateFormat("hh_mm_ss");
        String filename = format.format(dateNow)+".png";
        File screenshot = driver.getScreenshotAs(OutputType.FILE);

        try {
            FileUtils.copyFile(screenshot,
                    new File("C:\\WebDrivers\\"+filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void PrintScreenToFile()
    {
        try {
            PageObject.makePrintScreen((TakesScreenshot) this.driver);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
     protected void initPage()
     {
         driver.manage().window().maximize();
         driver.manage().deleteAllCookies();
         driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
         driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
     }

    public void go() {
        this.driver.get(this.getAddress());
        this.setAddress(this.driver.getCurrentUrl());
    }

    public boolean canUseElements()
    {
        return false;
    }

}
