import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

class LongTask
{
    private static final int n = 3;
    public void doLongAction(String title)
    {
        System.out.printf("Thread name is %s %n", Thread.currentThread().getName());
        for (int i = 0; i < n; i++) {
            System.out.printf("%s step %d %n", title,i);
        }
    }
}
class MyThread extends Thread{
    private String title;
    public MyThread(String title){this.title = title;}

    @Override
    public void run() {
        new LongTask().doLongAction(this.title);
    }
}
class Runner implements Runnable
{
    private String title;
    public Runner(String title){this.title = title;}
    @Override
    public void run() {
        new LongTask().doLongAction(this.title);
    }
}
class DaemonWorker implements Runnable
{
    @Override
    public void run() {
        while (true)
        {
            System.out.println("daemon is working...");
        }
    }
}
class WorkUpdater implements Runnable
{
    @Override
    public void run() {
        //HttpClient
        System.out.println("Update data ... ");
    }
}
//для JavaMailWeb
class Processor implements Callable<String>
{
    private int id;
    public Processor(int id){this.id = id;}
    @Override
    public String call() throws Exception {
        Thread.sleep(2000);
        return "Thread name: "+Thread.currentThread().getName()+", action Id: "+id;
    }
}
public class demoThreads {
    //+Thread Runnable (base from 1999)
    //+Executors (new for Parallels): +Scheduler, +FixedThreadPool
    //+Synchronized Collections: List - ArrayList
    public static void main(String[] args)
    {
        // List<Integer> nums = new ArrayList<>();
        List<Integer> nums = Collections.synchronizedList(new ArrayList<>());// ЛЮБАЯ коллекция становится потокобезопасной
        int n = 10;
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < n; i++) {
                    nums.add(i);
                }
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < n; i++) {
                    nums.add(i);
                }
            }
        });
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("size of array nums is "+nums.size());

    }
    public static void main_FixedThreadPool(String[] args)
    {
        ExecutorService service = Executors.newFixedThreadPool(2);//на двух процессорах
        List<Future<String>> responses = new ArrayList<>();
        int n = 7;
        for (int i = 0; i < n; i++) {
            Future<String> future = service.submit(new Processor(i+1));
            responses.add(future);
        }
        for (Future<String> f: responses
             ) {

            try {
                System.out.println(f.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        service.shutdown();
    }
    public static void main_executor_schedule(String[] args)
    {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        long initialDelay = 1000;// пауза до старта, регулярная работа по расписанию начнется через initialDelay
        long period = 5000;// периодичность, регулярность повторений
        executor.scheduleAtFixedRate(new WorkUpdater(),
                initialDelay,
                period,
                TimeUnit.MILLISECONDS);//единица измерения для интервалов
        executor.shutdown();// остановка сервиса, после окончания работы Worker
        //"аварийное выключение
        try {
            if (!executor.awaitTermination(1000, TimeUnit.MILLISECONDS))
            {
                executor.shutdownNow();
            }
        }catch (InterruptedException e)
        {
            executor.shutdownNow();
        }

    }
    public static void main_for_thread(String[] args)
    {
        System.out.println("start demoThreads ---------------------");
        LongTask task = new LongTask();
        task.doLongAction("call in main()");
        // 1. new Thread
        // 2. new instance of Runnable
        // 3. arrow function
//        Thread t0 = new Thread(new DaemonWorker());
//        t0.setDaemon(true);// по умолчанию false означает, что поток рабочий
//        t0.start();
        Thread t1 = new MyThread("My Thread");
        Thread t2 = new Thread( new Runner("My Runner"));
        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                new LongTask().doLongAction("thread run");
            }
        });
        t1.start();
        t2.start();
        t3.start();
        new Thread(()->{new LongTask().doLongAction("thread 2 run()");}).start();
    }
}
