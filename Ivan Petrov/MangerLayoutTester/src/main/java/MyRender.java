import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class MyRender extends JCheckBox implements TableCellRenderer {
    public  MyRender()
    {
        setOpaque(true);
    }
    @Override
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   int row,
                                                   int column) {
        boolean partTime = (boolean) value;
        if (partTime)
        {
            super.setBackground(Color.GREEN);
            super.setText("true");
            super.setSelected(partTime);
        }
        else if (partTime==false)
        {
            super.setBackground(Color.RED);
            super.setText("false");
            super.setSelected(partTime);
        }
        return this;
    }
}
