import javax.swing.table.DefaultTableModel;

public class MyTabelModel extends DefaultTableModel {

    private Class[] columnsTypes;

    public MyTabelModel(Object[][] data, String[] colums, Class[] columnsTypes) {
    super(data, colums);
    this.columnsTypes = columnsTypes;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return this.columnsTypes[columnIndex];
    }
}
