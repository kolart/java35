import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class MyClassTableModel extends AbstractTableModel {
    String[] colums = new String[]
            {
                    "id","Name","Rate","Part/Time"
            };
    Class[] columnsTypes = new Class[]
            {
                    Integer.class , String.class, Double.class, Boolean.class
            };
    List<MyClass> data;
    public MyClassTableModel()
    {
        this.data = new ArrayList<>();
        data.add( new MyClass(0,"Иванов", 40.0, true)  );
        data.add( new MyClass(1,"Петров", 70.0, false)  );
        data.add( new MyClass(2,"Сидоров", 60.0, true)  );
    }
    @Override
    public int getRowCount() {
        return this.data.size();
    }

    @Override
    public int getColumnCount() {
        return columnsTypes.length;// 4;//основываясь на массиве типов
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        MyClass row = this.data.get(rowIndex);
        if (0 == columnIndex)
        {
            return row.getId();
        }else if (1 == columnIndex)
        {
            return row.getName();
        }else if (2 == columnIndex)
        {
            return row.getRate();
        }else if (3 == columnIndex)
        {
            return row.isPartTime();
        }
        return row.getId();
    }
    //additional

    @Override
    public String getColumnName(int column) {
        return colums[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnsTypes[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        // return super.isCellEditable(rowIndex, columnIndex);
        return true;
    }
    //

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        MyClass row = this.data.get(rowIndex);
        if (0 == columnIndex)
        {
            row.setId((Integer)aValue);
        }else if (1 == columnIndex)
        {
            row.setName((String) aValue);
        }else if (2 == columnIndex)
        {
            row.setRate((Double)aValue);
        }else if (3 == columnIndex)
        {
            row.setPartTime((Boolean)aValue);
        }
    }
}
