public class MyClass {

    public int id;
    public String name;
    public double rate;
    public boolean partTime;

    public MyClass() {
    }

    public MyClass(int id, String name, double rate, boolean partTime) {
        this.id = id;
        this.name = name;
        this.rate = rate;
        this.partTime = partTime;
    }

    @Override
    public String toString() {
        return "MyClass{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", rate=" + rate +
                ", partTime=" + partTime +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public boolean isPartTime() {
        return partTime;
    }

    public void setPartTime(boolean partTime) {
        this.partTime = partTime;
    }
}
