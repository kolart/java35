import javax.swing.*;
import java.awt.*;

public class JTableTestWindow extends JFrame {
    private  JTable tblString;
    //JTable - view
    //DataModel

    //Data - из базы данных ==> Collection

   public JTableTestWindow()
   {
       this.setSize(400,300);
       this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       this.setTitle("Grid. JTable");

       //названия столбцов - массив
       //типы столбцов = массив
       //данные - в табличном виде

       String[] colums = new String[]
               {
                       "id","Name","Rate","Part/Time"
               };
       Class[] columnsTypes = new Class[]
               {
                 Integer.class , String.class, Double.class, Boolean.class
               };
       Object[][] data = new Object[][]
               {
                       {1,"Ivanov",40.0,false},
                       {2,"Petrov",30.0,true},
                       {3,"Sidorov",50,false}
               };

       // MyTabelModel model = new MyTabelModel(data, colums, columnsTypes);
       MyClassTableModel  model = new MyClassTableModel();
       this.tblString = new JTable(model);
       this.setLayout(new BorderLayout());
       this.add( new JScrollPane(  this.tblString ), BorderLayout.CENTER);
       this.tblString.setDefaultRenderer(Boolean.class, new MyRender());
       this.pack();


   }
}
