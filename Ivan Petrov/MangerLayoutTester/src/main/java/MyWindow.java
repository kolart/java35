import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyWindow extends JFrame implements ActionListener {

    private JLabel lblA;
    private JTextField txtA;
    private  JButton btnCalc;
    private  JLabel lblResult;

    public MyWindow()
    {
        this.setSize(400,400);
        this.setTitle("Вычисление площади");
        this.btnCalc = new JButton("ВЫЧИСЛИТЬ");
        this.btnCalc.addActionListener(this);
        this.lblA = new JLabel("a=");
        this.txtA = new JTextField("");
        this.lblResult = new JLabel("S = ");
        //BorderLayout, Gridlayout
        this.setLayout( new BorderLayout());
        JPanel panel1 = new JPanel();
        panel1.setLayout(new BorderLayout());
        this.add(panel1,BorderLayout.NORTH);
        panel1.add(lblA, BorderLayout.NORTH);
        panel1.add(txtA, BorderLayout.CENTER);
        panel1.add(this.btnCalc, BorderLayout.SOUTH);
        this.add(this.lblResult, BorderLayout.SOUTH);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.lblResult.setText("Clicked");
    }
}
