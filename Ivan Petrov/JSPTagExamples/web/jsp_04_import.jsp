<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="code.MyMath"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>Square of 3 = <%= new MyMath().square(3) %></h2>
        <h2>Sqrt of 16 = <%= MyMath.sqrt(3) %></h2>
    </body>
</html>
