<%-- 

Выражения JSP (JSP Expression)

Скриплет JSP (JSP Scriplet)

Объявления JSP (JSP Declaration)

Директивы JSP (JSP Directive)

Комментарии JSP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page Expression</title>
    </head>
    <body>
        <%-- 
        JSP Expression представляет выражение, заключенное между тегами <%= и %>. При обращении к JSP вычисляется значение этого выражения.
        --%>
        <h1>Jsp Expression: </h1>
        <p>2 + 2 = <%= 2 + 2 %></p>
        <p>5 > 2 = <%= 5 > 2 %></p>
        <p><%= new String("Hello").toUpperCase() %></p>
        <p>Today <%= new java.util.Date() %></p>
    </body>
</html>
