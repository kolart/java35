<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String[] people = new String[]{"Tom", "Bob", "Sam"};
    String header = "Users list";
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page JSP Scriplet</title>
    </head>
    <body>
        <h1>JSP Scriplet</h1>
        <%
            for(int i = 1; i < 5; i++){
                out.println("<br>Hello " + i);
            }
        %>
        
        <h3><%= header %></h3>
        <ul>
        <%
            for(String person: people){
                out.println("<li>" + person + "</li>");
            }
        %>
        </ul>
    </body>
</html>

