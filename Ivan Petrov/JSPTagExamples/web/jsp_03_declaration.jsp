<%-- 

JSP Declaration позвол¤ют определить метод,
который мы затем можем вызывать в скриплетах или в JSP-выражени¤х.
ќпределение метода помещаетс¤ между тегами <%! и %>. Ќапример, определим следующую 
функцию square (НЕ статический)
JSP-страницу:
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%!
    int square(int n){
        return n * n;
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>declaration</h1>
        
        <p><%= square(6) %></p>
        <ul>
        <%
            for(int i = 1; i <= 5; i++){
                out.println("<li>" + square(i) + "</li>");
            }
        %>
        </ul>
        
    </body>
</html>

