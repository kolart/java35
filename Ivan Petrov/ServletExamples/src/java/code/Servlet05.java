/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package code;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 
 Куки представляют простейший способо хранения данных приложения. Куки хранятся в браузере польвователя в виде пары ключ-значение: с каждым уникальным ключом сопоставлется определенное значение. По ключу мы можем получить сохраненное в куках значение. Приложение на сервере может устанавливать куки и отправлять в ответе пользователю, после чего куки сохраняются в браузере. Когда клиент отправляет запроск приложению, то в запросе также отправляются и те куки, которые установленны данным приложением.

Куки могут быть двух типов. Одни куки хранятся только в течении сеанса. То есть когда пользователь закрывает вкладку браузера и прекращает работать с приложением, то куки сеанса уничтожаются. Второй тип куков - постоянные куки - хранятся в течение продолжительного времени (до 3 лет).

Следует учитывать некоторые ограничения. Прежде всего куки нередко ограничены по размеру (обычно не более 4 килобайт). Кроме того, обычно браузеры принимают не более 20 кук с одного сайта. Более того, в некоторых браузерах может быть отключена поддержка кук.

Для работы с куками сервлеты могут используют класс javax.servlet.http.Cookie. Для создания куки надо создать объект этого класса с помощью констуктора Cookie(String name, String value), где name - ключ, а value - значение, которое сохраняется в куках. Стоит отметить, что мы можем сохранить в куках только строки.

Чтобы добавить куки в ответ клиенту у объекта HttpServletResponse применяется метод addCookie(Cookie c)

При создании куки мы можем использовать ряд методов объекта Cookie для установки и получения отдельных параметров:

setMaxAge(int maxAgeInSeconds): устанавливает время в секундах, в течение которого будут существовать куки. Специальное значение -1 указывает, что куки будут существовать только в течение сессии и после закрытия браузера будут удалены.

setValue(String value): устанавливает хранимое значение.

getMaxAge(): возвращает время хранения кук.

getName(): возвращает ключ кук.

getValue(): возвращает значение кук.
* 
Чтобы получить куки, которые приходят в запросе от клиента, применяется метод getCookies() класса HttpServletRequest.
 */
@WebServlet("/servlet05")
public class Servlet05 extends HttpServlet {
  
    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
       Cookie[] cookies = request.getCookies();
        String cookieName = "user";
        Cookie cookie = null;
        if(cookies !=null) {
            for(Cookie c: cookies) {
                if(cookieName.equals(c.getName())) {
                    cookie = c;
                    break;
                }
            }
        }
        PrintWriter out = response.getWriter();
        try {
            out.println("Name: " + cookie.getValue());
        }
        finally {
            out.close();
        }
        
       
    }
}
