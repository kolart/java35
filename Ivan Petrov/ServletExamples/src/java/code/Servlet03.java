
package code;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/servlet03")
public class Servlet03 extends HttpServlet{
     protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        if (id == null) {
              /*
    Перенаправление запроса
Метод forward() класса RequestDispatcher позволяет перенаправить запрос из сервлета на другой сервлет, 
    html-страницу или страницу jsp. 
    Причем в данном случае речь идет о перенаправлении запроса, а не о переадресации.
    
    */
            /*
            Для того, чтобы выполнить перенаправление запроса, вначале с помощью метода getServletContext() 
            получаем объект ServletContext, который представляет контекст запроса. 
            Затем с помощью его метода getRequestDispatcher() получаем объект RequestDispatcher. 
            Путь к ресурсу, на который надо выполнить перенаправление, передается в качестве параметра в getRequestDispatcher.

            Затем у объекта RequestDispatcher вызывается метод forward(), 
            в который передаются объекты HttpServletRequest и HttpServletResponse.
            */
            String path = "/forward.html";
            ServletContext servletContext = getServletContext();
            RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(path);
            requestDispatcher.forward(request, response);
        
        }else if (id.contains("4")){
            
             String path = "/servlet04";
            ServletContext servletContext = getServletContext();
            RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(path);
            requestDispatcher.forward(request, response);
            
        }else if (id.contains("5")){
        
            String path = request.getContextPath() + "/servlet05";
            response.sendRedirect(path);
        }
        else {

            response.setContentType("text/html");
            PrintWriter writer = response.getWriter();
            try {
                writer.println("<h2>Hello Id " + id + "</h2>");
            } finally {
                writer.close();
            }
        }
    }
}
