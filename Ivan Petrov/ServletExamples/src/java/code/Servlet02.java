package code;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet02")
public class Servlet02 extends HttpServlet {

    // обработка пути /servlet02?id=5&name=Alex&age=33&nums=1&nums=2&nums=3
    //параметры id name age и массив nums
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String age = request.getParameter("age");
        String[] nums = request.getParameterValues("nums");
        try {
            writer.println("<h2>id:"+id+"</h2>");
            writer.println("<h2>name:"+name+"; age:"+age+"</h2>");
            writer.println("<h2>numbers: ");
                    
               for(String n: nums)
                    writer.print(n + " ");
                    
            writer.println("</h2>");
            
        } finally {
            writer.close();  
        }
    }

}
