
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Примеры создания сервлетов</h1>
          <ul>
            <li>
                <a href="./servlet01">Пример простого Сервлета</a>
            </li>
            <li>
                <a href="./servlet02?id=5&name=Alex&age=33&nums=1&nums=2&nums=3">Получение данных в сервлете Get - запрос</a>
            </li>
           
            <li>
                <a href="./servlet03">Переадресация и перенаправление запроса</a>
            </li>
                      
            <li>
                <a href="./servlet04">Установить Куки</a>
            </li>
            <li>
                <a href="./servlet05">Получить Куки</a>
            </li>
            <li>
                <a href="./servlet06">Сессии</a>
            </li>
        </ul>
    </body>
</html>
