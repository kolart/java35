import java.util.Locale;
import java.util.ResourceBundle;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
//
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;

public class JavaLocale {
    public static void main(String[] args)
    {
        System.out.println("Hello from JavaLocale");
        //
        Locale current = Locale.getDefault();
        System.out.println(current);
        System.out.println(current.getCountry());
        System.out.println(current.getDisplayCountry());
        System.out.println(current.getLanguage());
        System.out.println(current.getDisplayLanguage());
        //
        String country = "", language = "";
        System.out.println("1 – Английский");
        System.out.println("2 – Белорусский");
        System.out.println("3 – Русский");
        System.out.println("4 – ukrainian");
        char i = 0;
        try {
            i = (char) System.in.read();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        switch (i) {
            case '1':
                country = "US";
                language = "EN";
                break;
            case '2':
                country = "BY";
                language = "BEL";
                break;
            case '3':
                country = "ru";
                language = "RU";
                break;
            case '4':
                country = "UA";
                language = "uk";
                break;
            //http://www.localeplanet.com/icu/uk-UA/index.html
            //Потому что формат записи язык_страна. en_US, en_UK, ru_RU, uk_UA, ru_UA (русский без поребриков)
            //Потому что UK(country) — это United Kingdom, у которой язык En
            //uk_UA, а не ua_UA
            //язык_страна, uk — код языка согласно ISO 639-1
            //https://dou.ua/forums/topic/21260/
        }
        //new Locale("ru", "RU"),
        current = new Locale(language, country);
        System.out.println(current);
        ResourceBundle rb
                = ResourceBundle.getBundle("text", current);
        try {
            String st = rb.getString("str1");
            String s1 = new String(st.getBytes("ISO-8859-1"), "UTF-8");
            System.out.println(s1);
            st = rb.getString("str2");
            String s2 = new String(st.getBytes("ISO-8859-1"), "UTF-8");
            System.out.println(s2);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //
        System.out.println("Кодировка поумолчанию после замены: ");
        Locale.setDefault(current);
        System.out.println(Locale.getDefault());
        //
        ArrayList<String> countries = new ArrayList<String>();
        countries.add("Єгипет");
        countries.add("Туреччина");
        countries.add("Ісландія");
        countries.add("Аргентина");
        System.out.println("Before sort:");
        System.out.println(countries);
        //
        System.out.println("After regular sort:");
        Collections.sort(countries);
        System.out.println(countries);
        //
        Locale ukrainian = new Locale("uk", "UA");
        System.out.println(ukrainian);
        Collator ukrainianCollator = Collator.getInstance(ukrainian);
        Collections.sort(countries, ukrainianCollator);
        System.out.println("After locale sort:");
        System.out.println(countries);
        //

        //
        //
    }
}
