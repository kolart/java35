package com.sourceit.manytomanyjpamapping;

import java.util.Set;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity 
@Table(name = "Employee") 
public class Employee {

//    public Employee(String Name, String work_phone, double salary) {
//        this.tab_number = Name;
//        this.work_phone = work_phone;
//        this.salary = salary;
//    }

    public Employee() {
    }

    Employee(String name) {
        this.name = name;//
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", departments=" + departments + '}';
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

  

//    public String getWork_phone() {
//        return work_phone;
//    }
//
//    public void setWork_phone(String work_phone) {
//        this.work_phone = work_phone;
//    }
//
//    public double getSalary() {
//        return salary;
//    }
//
//    //
//    public void setSalary(double salary) {
//        this.salary = salary;
//    }
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    private long id = 1L;

//    public String getTab_number() {
//        return tab_number;
//    }
//
//    public void setTab_number(String tab_number) {
//        this.tab_number = tab_number;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    String name;
    // Ошибка
//    private String tab_number;
//    private String work_phone;
//    private double salary;

    public Set<Department> getDepartments() {
        return departments;
    }

    //
    public void setDepartments(Set<Department> departments) {
        this.departments = departments;
    }
   /*
     Двунаправленный маппинг many-to-many

Если сущности типов Department и Employees будут иметь ссылки на коллекции друг друга, то отношения будут двунаправленными.
Для этого:
    создадим поле departments типа List<Department> в этом классе Employees
    добавим к этому полю departments аннотацию @ManyToMany
    добавим элемент аннотации mappedBy, который будет указывать на имя поля класса Department,
    которое в том класск имеет аннотации @ManyToMany и @JoinTable, т.е. departments
    */
    @ManyToMany(mappedBy = "employees")//employees - это поле в классе Department
    private Set<Department> departments;
    
    
}
