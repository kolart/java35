package mathfunctionstest;


public class MyMath {
    
    public static double calcGeron(double a, double b, double c)
    {
        double p = (a + b + c) / 2.0;
        double area = Math.sqrt(p * (p - a) * (p - b) * (p - c));
        return area;
    }
    //false D < 0, true
    //C# out double x1, out double x2
    //C++ double& x1, double& x2;
    // ссылочный тип данных
    // []
    // Object => class
    // String
    public boolean calcQuadraticEquation(double a,
                                         double b,
                                         double c, 
                                         double[] solutions)
    {
        double D = b * b - 4 * a * c;
        boolean result = true;
        if ((D >= 0) && (solutions != null) && (solutions.length == 2))
        {
            solutions[0] = (- b + Math.sqrt(D)) / (2 * a);
            solutions[1] = (- b - Math.sqrt(D)) / (2 * a);
            result = true;
        }
//        else
//        {
//           result = false;
//        }
        return !result;
    }

}
