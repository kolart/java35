package mathfunctionstest;


public class MathFunctionsTest {

  
    public static void main(String[] args) {
       double A = 3;
       double B = 4;
       double C = 5;
       double S = MyMath.calcGeron(A, B, C);
       System.out.println("S = " + S);
       double a = 1;
       double b = -7;
       double c = 12;
       double[] solutions = new double[2];
       MyMath math = new MyMath();
       boolean result = math.calcQuadraticEquation(a, b, c, solutions);
       if (result)
       {
           System.out.println("X1 = "+solutions[0] + " X2 = "+solutions[1]);
       }
       
    }
    
}
