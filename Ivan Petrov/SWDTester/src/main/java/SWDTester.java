import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
// for HW import  org.openqa.selenium.chrome.ChromeDriver;

import org.apache.commons.io.FilenameUtils;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

public class SWDTester {
    //ДЗ
    // Создание и вызов драйвера для chrom, edge
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Hello from SWDTester");
        //+1. Создание и вызов драйвера для firefox
        //тест для сайтов google и wikipedia
        //+2. Как делать скриншоты экранов

        //1.
        //ключ+путь для каждого из WebDriver
        System.setProperty("webdriver.gecko.driver","C:\\WebDrivers\\geckodriver.exe"); // Setting system properties of FirefoxDriver
        WebDriver driver = new FirefoxDriver();
        // same way:
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        // testGoogleSearch(driver);
        testWikiSearch(driver);
        Date dateNow = new Date();
        SimpleDateFormat format = new SimpleDateFormat("hh_mm_ss");
        String filename = format.format(dateNow)+".png";
        File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        try {
            FileUtils.copyFile(screenshot,
                    new File("C:\\WebDrivers\\"+filename));
        } catch (IOException e) {
            e.printStackTrace();
        }


        Thread.sleep(5000);// 5 sec pause
        System.out.println("Finished.");

    }

    private static void testWikiSearch(WebDriver driver) {
        String url = "https://en.wikipedia.org/wiki/Main_Page";
        driver.get(url);
        System.out.println("Заголовок страницы "+ driver.getTitle());
        System.out.println("загруженный адрес (url) сайта "+ driver.getCurrentUrl());
        //  searchInput
        // WebElement input = driver.findElement(By.xpath("//input[@id=\"searchInput\"]"));
        WebElement input = driver.findElement(By.id("searchInput"));

        if (input != null)
        {
            input.sendKeys("google"+ Keys.ENTER);
        }
    }

    private static void testGoogleSearch(WebDriver driver) {
        String url = "https://www.google.com/";
        driver.get(url);
        System.out.println("Заголовок страницы "+ driver.getTitle());
        System.out.println("загруженный адрес (url) сайта "+ driver.getCurrentUrl());

        WebElement input = driver.findElement(By.name("q"));

        if (input != null)
        {
            input.sendKeys("wiki"+Keys.ENTER);
        }

        System.out.println("Заголовок страницы "+ driver.getTitle());
        System.out.println("загруженный адрес (url) сайта "+ driver.getCurrentUrl());
    }
}
